-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 08, 2019 at 10:05 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `link` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `page` varchar(30) NOT NULL,
  `start_at` int(11) NOT NULL,
  `end_at` int(11) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`id`, `name`, `link`, `image`, `page`, `start_at`, `end_at`, `status`) VALUES
(1, 'google', 'www.google.com', 'ce6229a125deee5ee06f65bbd1e125d1a6c031bb_c9be92112121388fa5ff150b0ff78913c704e6b1.jpg', '/', 1554501600, 1554847200, 'enabled');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `name`, `status`) VALUES
(1, 0, 'sport', 'enabled'),
(2, 0, 'computers', 'enabled'),
(11, 0, 'test', 'enabled'),
(12, 0, 'test category', 'enabled'),
(13, 0, 'category', 'enabled');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `created_at` int(11) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `post_id`, `comment`, `created_at`, `status`) VALUES
(1, 1, 1, 'test comment', 0, 'enabled');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `cont_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(40) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `online_users`
--

CREATE TABLE `online_users` (
  `online_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `users_group_id` int(11) NOT NULL,
  `page` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `users_group_id`, `page`) VALUES
(1, 1, '/admin/login'),
(2, 1, '/admin/login/submit'),
(3, 1, '/admin/users'),
(4, 1, '/admin/users/add'),
(5, 1, '/admin/users/submit'),
(6, 1, '/admin/users/edit/:id'),
(7, 1, '/admin/users/save/:id'),
(8, 1, '/admin/users/delete/:id'),
(9, 1, '/admin/users_groups'),
(10, 1, '/admin/users_groups/add'),
(11, 1, '/admin/users_groups/submit'),
(12, 1, '/admin/users_groups/edit/:id'),
(13, 1, '/admin/users_groups/save/:id'),
(14, 1, '/admin/users_groups/delete/:id'),
(15, 1, '/admin/posts'),
(16, 1, '/admin/posts/add'),
(17, 1, '/admin/posts/submit'),
(18, 1, '/admin/posts/edit/:id'),
(19, 1, '/admin/posts/save/:id'),
(20, 1, '/admin/posts/delete/:id'),
(21, 1, '/admin/posts/:id/comments'),
(22, 1, '/admin/comments/edit/:id'),
(23, 1, '/admin/comments/save/:id'),
(24, 1, '/admin/comments/delete/:id'),
(25, 1, '/admin/ads'),
(26, 1, '/admin/ads/add'),
(27, 1, '/admin/ads/submit'),
(28, 1, '/admin/ads/edit/:id'),
(29, 1, '/admin/ads/save/:id'),
(30, 1, '/admin/ads/delete/:id'),
(31, 1, '/admin/categories'),
(32, 1, '/admin/categories/add'),
(33, 1, '/admin/categories/submit'),
(34, 1, '/admin/categories/edit/:id'),
(35, 1, '/admin/categories/save/:id'),
(36, 1, '/admin/categories/delete/:id'),
(37, 1, '/admin/settings'),
(38, 1, '/admin/settings/save'),
(39, 1, '/admin/contacts'),
(40, 1, '/admin/contacts/reply/:id'),
(41, 1, '/admin/contacts/send/:id'),
(42, 1, '/admin/logout'),
(43, 1, '/admin'),
(44, 1, '/admin/dashboard'),
(45, 1, '/admin/submit'),
(46, 2, '/admin/users/submit'),
(47, 2, '/admin/users/edit/:id'),
(48, 2, '/admin/users/save/:id'),
(49, 2, '/admin/users/delete/:id'),
(50, 2, '/admin/users_groups'),
(51, 2, '/admin/users_groups/add'),
(52, 2, '/admin/users_groups/submit'),
(53, 2, '/admin/users_groups/edit/:id'),
(54, 2, '/admin/users_groups/save/:id'),
(55, 2, '/admin/users_groups/delete/:id'),
(56, 2, '/admin/posts'),
(57, 2, '/admin/posts/add'),
(58, 2, '/admin/posts/submit'),
(59, 2, '/admin/posts/edit/:id'),
(60, 2, '/admin/posts/save/:id'),
(61, 2, '/admin/posts/delete/:id'),
(62, 2, '/admin/posts/:id/comments'),
(63, 2, '/admin/comments/edit/:id'),
(64, 2, '/admin/comments/save/:id'),
(65, 2, '/admin/comments/delete/:id'),
(66, 2, '/admin/ads'),
(67, 2, '/admin/ads/add'),
(68, 2, '/admin/ads/submit'),
(69, 2, '/admin/ads/edit/:id'),
(70, 2, '/admin/ads/save/:id'),
(71, 2, '/admin/ads/delete/:id'),
(72, 2, '/admin/categories'),
(73, 2, '/admin/categories/add'),
(74, 2, '/admin/categories/submit'),
(75, 2, '/admin/categories/edit/:id'),
(78, 2, '/admin/settings'),
(79, 2, '/admin/settings/save'),
(80, 2, '/admin/contacts'),
(81, 2, '/admin/contacts/reply/:id'),
(82, 2, '/admin/contacts/send/:id'),
(83, 2, '/admin/logout'),
(84, 2, '/admin'),
(85, 2, '/admin/dashboard'),
(86, 2, '/admin/submit'),
(90, 3, '/admin/login/submit');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `tags` text NOT NULL,
  `related_posts` text NOT NULL,
  `views` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `cat_id`, `title`, `details`, `image`, `tags`, `related_posts`, `views`, `created_at`, `status`) VALUES
(1, 0, 11, 'post for test', '&lt;p style=&quot;text-align:right&quot;&gt;&amp;nbsp;bttttttttttttttgb&lt;img alt=&quot;angry&quot; src=&quot;http://blog.localhost/public/admin/ckeditor/plugins/smiley/images/angry_smile.png&quot; style=&quot;height:23px; width:23px&quot; title=&quot;angry&quot; /&gt;&lt;/p&gt;\r\n', '4e1bdde10a87a6ffaa94b08381ea6d8973c19e69_3e9bb1169edd26cec41f83edd81ff0ba3c8edec0.jpg', 'tag,ret,fff', '3', 0, 1554337342, 'disenabled'),
(2, 1, 12, ' test post num 2', '&lt;p&gt;&lt;span style=&quot;color:#e74c3c&quot;&gt;test for second time&amp;nbsp;&lt;img alt=&quot;smiley&quot; src=&quot;http://blog.localhost/public/admin/ckeditor/plugins/smiley/images/regular_smile.png&quot; style=&quot;height:23px; width:23px&quot; title=&quot;smiley&quot; /&gt;&amp;nbsp;test for second time&amp;nbsp;&lt;img alt=&quot;smiley&quot; src=&quot;http://blog.localhost/public/admin/ckeditor/plugins/smiley/images/regular_smile.png&quot; style=&quot;height:23px; width:23px&quot; title=&quot;smiley&quot; /&gt;test for second time&amp;nbsp;&lt;img alt=&quot;smiley&quot; src=&quot;http://blog.localhost/public/admin/ckeditor/plugins/smiley/images/regular_smile.png&quot; style=&quot;height:23px; width:23px&quot; title=&quot;smiley&quot; /&gt;test for second time&amp;nbsp;&lt;img alt=&quot;smiley&quot; src=&quot;http://blog.localhost/public/admin/ckeditor/plugins/smiley/images/regular_smile.png&quot; style=&quot;height:23px; width:23px&quot; title=&quot;smiley&quot; /&gt;&lt;/span&gt;&lt;/p&gt;\r\n', '38341f16915cbc2cf8bd96f89c0a0e8fa4873eec_ea25c64cb4df36cd54aaffab2014543ed7fa5ea2.jpg', 'tag,fff', '4', 0, 1554339368, 'enabled'),
(3, 7, 2, 'test post', '&lt;p&gt;&lt;span style=&quot;color:#27ae60&quot;&gt;it is long text for text number of characters in input&amp;nbsp;it&amp;nbsp;it is long text for text number of characters in input&amp;nbsp;&amp;nbsp;it is long text for text number of characters in input&amp;nbsp;&amp;nbsp;it is long text for text number of characters in input&amp;nbsp;&amp;nbsp;it is long text for text number of characters in input&amp;nbsp; is long text for text number of characters in input&amp;nbsp;&lt;img alt=&quot;cool&quot; src=&quot;http://blog.localhost/public/admin/ckeditor/plugins/smiley/images/shades_smile.png&quot; style=&quot;height:23px; width:23px&quot; title=&quot;cool&quot; /&gt;&lt;/span&gt;&lt;/p&gt;', '99ebc6fad69a0249bf387bc9fd0a1e6126eaa7c7_8efb05fe77ff909d2db1457ca25b990ec4a03f78.jpg', 'test', '', 0, 1554670938, 'enabled'),
(4, 7, 2, 'test post', '&lt;p&gt;&lt;span style=&quot;color:#27ae60&quot;&gt;it is long text for text number of characters in input&amp;nbsp;it&amp;nbsp;it is long text for text number of characters in input&amp;nbsp;&amp;nbsp;it is long text for text number of characters in input&amp;nbsp;&amp;nbsp;it is long text for text number of characters in input&amp;nbsp;&amp;nbsp;it is long text for text number of characters in input&amp;nbsp; is long text for text number of characters in input&amp;nbsp;&lt;img alt=&quot;cool&quot; src=&quot;http://blog.localhost/public/admin/ckeditor/plugins/smiley/images/shades_smile.png&quot; style=&quot;height:23px; width:23px&quot; title=&quot;cool&quot; /&gt;&lt;/span&gt;&lt;/p&gt;', 'ea110f18a27e4e6043f58d2bc34496ba91771653_054bbd288c8911427d9115ceaae319522782c5f8.jpg', 'test', '', 0, 1554670948, 'enabled'),
(5, 7, 12, 'ssssssssssssssss', '&lt;p&gt;xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx&lt;/p&gt;', 'dbaec83c1121fa1e3b817e3c522b98e50c0b649e_4b598e9133b68a756ee20e2c9e16a7218a061e2f.jpg', 'test', '2', 0, 1554671100, 'enabled'),
(6, 7, 1, 'new test post ', '&lt;p&gt;&lt;span style=&quot;color:#27ae60&quot;&gt;it is long text for text number of characters in input&amp;nbsp;it&amp;nbsp;it is long text for text number of characters in input&amp;nbsp;&amp;nbsp;it is long text for text number of characters in input&amp;nbsp;&amp;nbsp;it is long text for text number of characters in input&amp;nbsp;&amp;nbsp;it is long text for text number of characters in input&amp;nbsp; is long text for text number of characters in input&amp;nbsp;&lt;img alt=&quot;cool&quot; src=&quot;http://blog.localhost/public/admin/ckeditor/plugins/smiley/images/shades_smile.png&quot; style=&quot;height:23px; width:23px&quot; title=&quot;cool&quot; /&gt;&lt;/span&gt;&lt;/p&gt;\r\n', '47842ae6284c00b89496fbc3d65a40ff32870a4e_82fd3b2799d0c9a0fd5f16a8f054585e3ca53c72.jpg', 'test', '2', 0, 1554671100, 'enabled');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `s_key` varchar(40) NOT NULL,
  `s_value` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `s_key`, `s_value`) VALUES
(25, 'site_name', 'blog site'),
(26, 'site_email', 'blog@blog.test'),
(27, 'status', 'enabled'),
(28, 'message', '&lt;p style=&quot;text-align:center&quot;&gt;&lt;strong&gt;Blog Message&amp;nbsp;&lt;img alt=&quot;b');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `users_group_id` int(11) NOT NULL,
  `f_name` varchar(40) NOT NULL,
  `l_name` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(80) NOT NULL,
  `gender` varchar(5) NOT NULL,
  `birthday` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `ip` varchar(30) NOT NULL,
  `code` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `users_group_id`, `f_name`, `l_name`, `email`, `password`, `gender`, `birthday`, `image`, `created_at`, `status`, `ip`, `code`) VALUES
(1, 1, 'administrator', 'admin', 'admin@admin.com', '$2y$10$uQz6NOoRVKrwiUnVA0MqfuVjad3ep7N8hphXN.VpDHKOletDx8oiW', '', 0, '8baefdafa865dfbbbe7a3f2613d777d00b923d72_7f39a14fe309388cae3e5f8b40e880913e1f873a.jpg', 4554444, '', '', '$2y$10$sc4SKGsIOLoR/VOChD/Dee9.GUIt2Kcvf'),
(4, 3, 'sam', 'ali', 'sam@test.com', '$2y$10$HHBcVH5OjSkNhTFj7NZrJuJIahywTmao/LmNWLiG6AsOYDU6dCkRG', 'male', 789865200, 'c7c126c58168539fe982eace09ba33e0f07242b5_c3a73268206b413de8bd04771d3a6908f5ba29bd.jpg', 1554327914, 'enabled', '::1', 'dd6fd2f6ea8b3cc349a6c22f64b1e74b39e6990e'),
(5, 3, 'asdqq', 'dsaq', 'asd@asqqd.com', '$2y$10$9tDEw6VbwxK4qQFEHHHYFekiVNbkodV7wFPXRHjqyAl/lHWs8Rovm', 'male', 1146348000, 'd5707edbc593b153feb251b3f532286a9c5e755a_dada050e6cdbfd051df650c962790517ec2b5bcb.jpg', 1554329229, 'enabled', '::1', '1004610e72ae6b7736297097c6a6cc03febd3a7e'),
(7, 3, 'aml', 'ahmed', 'aml@gmail.com', '$2y$10$yTO34NCWuKi5exxS.tBfweg9XAzcazB9izdgFLwBvUW2BsEzhscfi', 'femal', 0, '7f23166dbf74d22709e3c6f66d398f5b7e968069_b705ca73d0accbb931d313e67f28fe2ea4a819bf.jpg', 1554655477, '', '::1', '16e7199f2cab75c96c30a3d28fcf3adab595ff5f'),
(8, 3, 'sam', 'ahmed', 'sam@gmail.com', '$2y$10$EUUOfvAeFFhOjPyVaOD6qOVqjoz7Hh0mrlVCYNx6iFHyY2WzWUKSW', 'male', 0, '8221803db3da00283c195a8cd84d767d9e9dd400_5d0a93516c6ed26f4250be8077235381bf036371.jpg', 1554655559, '', '::1', 'ab9005b98765d04d4d6c55eb218dcd90bbc1d5f7');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `name`) VALUES
(1, 'Super Administrators'),
(2, 'admin'),
(3, 'users');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`cont_id`);

--
-- Indexes for table `online_users`
--
ALTER TABLE `online_users`
  ADD PRIMARY KEY (`online_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `cont_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `online_users`
--
ALTER TABLE `online_users`
  MODIFY `online_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
