<?php 
namespace App\Controllers\admin;

use System\Controller;
use System\Database;

class AccessController extends Controller
{
	/**
	*check user permission to access admin pages 
	*@return void
	*/
	public function index()
	{   
		$loginModel = $this->load->model('Login');
		
        $ignoredPages = ['/admin/login' , '/admin/login/submit'];
        
        $currentRoute = $this->route->getCurrentRouteUrl();

        /**
        *first scenario:
        *user in not logged in and he is not requesting login page 
        *then we will redirect him to login page
        */
		if($isNotLogged = !$loginModel->isLogged() AND  !in_array($currentRoute, $ignoredPages))
		{
			return $this->url->redirectTo('/admin/login');
		}

		/*
		*on going to thsi line it means two possiblities
		*one : user in not logged in and he is requesting login page 
		*two : user logged in successfull and he is requesting admin page 
		*/
		if(!$isNotLogged)
		{
			return false;
		}

		$user = $loginModel->user();
        if($user)
        {
        	$usersGroupsModel = $this->load->model('UsersGroup');
			$usersGroup = $usersGroupsModel->get($user['users_group_id']);
	         /**
	         *if user dosen't has permission to access this page then we will redirect him to /
	         */

	        if(! in_array($currentRoute, $usersGroup['pages']))
	        {
	        	return $this->url->redirectTo('/');
	        }
        }
		
    } 

}
?>