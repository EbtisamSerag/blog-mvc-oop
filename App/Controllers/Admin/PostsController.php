<?php 
namespace App\Controllers\admin;

use System\Controller;
use System\Database;

class PostsController extends Controller
{
	/**
	*display posts  list
	*@return mixed
	*/
	public function index()
	{
		$this->html->setTitle('Posts');
		$data['posts'] = $this->load->model('Post')->all();

		$data['success'] = $this->session->has('success') ? $this->session->pull('success') :null;
		
		$view = $this->view->render('admin/posts/list',$data);
		return $this->adminLayout->render($view);
	}

	/**
	*open posts  form
	*@return string
	*/ 
	public function add()
	{
		return $this->form();
	}



	/**
	*submit for creating new post
	*@return string|json
	*
	*/
	public function submit()
	{ 
		$json = [];
		if($this->isValid())
		{
			$this->load->model('Post')->create();

		    $json['success'] = 'Post Has Been Added Successfully';
		    $json['redirectTo'] = $this->url->link('admin/posts');


		}else{
			
			$json['errors'] = $this->validator->flattenMessages();
		}
		return json_encode($json);
	}

	/**
	*display edit form
	*@param int $id
	*@return string
	*/
	public function edit($id)
	{
		$postModel = $this->load->model('Post');
        
		if(! $postModel->exists($id))
		{
			return $this->url->redirectTo('/404');
		}
		
		$post = $postModel->get($id);
		

        return $this->form($post);
		
		
 	}

 	/**
 	*display form
 	*/
 	private function form($post = null)
 	{
 		if($post)
 		{
 			//edit form 
 			$data['target'] = 'edit-post-' .$post->id;
 			$data['action'] = $this->url->link('/admin/posts/save/' . $post->id);
 			$data['heading'] = 'Edit ' . $post->title;
 		}else{
 			//add form
 			$data['target'] = 'add-post-form';
 			$data['action'] = $this->url->link('/admin/posts/submit');
 			$data['heading'] = 'Add Post ';
 		}
 		$post = (array) $post;
 		$data['title'] = array_get($post ,'title');
 		$data['cat_id'] = array_get($post ,'cat_id');
 		$data['status'] = array_get($post ,'status','enabled');
 		$data['details'] = array_get($post ,'details');
 		$data['tags'] = array_get($post ,'tags');
 		$data['id'] = array_get($post ,'id');

        
 		$data['image'] = '';
 		$data['related_posts'] = [];

 		if(!empty($post['image']))
 		{
 			//default path to upload post image : public/uploads/images
 			$data['image'] = $this->url->link('Public/uploads/images/' . $post['image']);
 		}

 		if($post['related_posts'])
 		{

 			$data['related_posts'] = explode(',', $post['related_posts']);
 			
 		}
 		
 		$data['categories'] = $this->load->model('Category')->all();
 		$data['posts'] = $this->load->model('Post')->all();

        
 		return $this->view->render('admin/posts/form' ,$data);

 	} 


 	/**
	*submit for creating new post
	*@return string|json
	*
	*/
	public function save($id)
	{
        $json = [];
		if($this->isValid($id))
		{
			$this->load->model('Post')->update($id);

		    $json['success'] = 'Post Has Been Updated Successfully';
		    $json['redirectTo'] = $this->url->link('admin/posts');


		}else{
			
			$json['errors'] = $this->validator->flattenMessages();
		}
		return json_encode($json);
	}

	/**
	*delete record
	*@param in $id
	*@return mixed
	*/
	public function delete($id)
	{
		$postModel = $this->load->model('Post');
        
		if(! $postModel->exists($id))
		{
			return $this->url->redirectTo('/404');
		}
		$postModel->delete($id);
		$this->session->set('success', 'Post Has Been Deleted Successfully') ;
		return $this->url->redirectTo('/admin/posts');
	}

	/**
	*validate the form
	*@param int $id
	*@return bool
	*/
	private function isValid($id = null)
	{
		$this->validator->required('title');
		$this->validator->required('details');
		$this->validator->required('tags');
        
        if(is_null($id))
        {    
        	$this->validator->requiredFile('image')->image('image');
        }else{
        	$this->validator->image('image');
        }
		return $this->validator->passes();
	}

}
?>