<?php 
namespace App\Controllers\admin;

use System\Controller;
use System\Database;

class ProfileController extends Controller
{


 	/**
	*submit for creating new user
	*@return string|json
	*
	*/
	public function update()
	{
        $json = [];

        //we will get current user object from the login model to get his id
        $user = $this->load->model('Login')->user();

		if($this->isValid($user->id))
		{    
			$usersGroupId = $user->users_group_id;
			$this->load->model('User')->update($user->id,$usersGroupId);

		    $json['success'] = 'User Has Been Updated Successfully';
		    $json['redirectTo'] = $this->request->referer() ?:$this->url->link('admin/users');


		}else{
			
			$json['errors'] = $this->validator->flattenMessages();
		}
		return json_encode($json);
	}

	/**
	*validate the form
	*@param int $id
	*@return bool
	*/
	private function isValid($id = null)
	{
		$this->validator->required('f_name','First Name is Required');
		$this->validator->required('l_name','Last Name is Required');
		$this->validator->required('email')->email('email');

        if($this->request->post('passord'))
        {
            $this->validator->required('password')
        	           ->minlen('password',8)
        	           ->match('password','conf_password','Confirm Password should match with password');
        }
       
       $this->validator->image('image');
       
		return $this->validator->passes();
	}

}
?>