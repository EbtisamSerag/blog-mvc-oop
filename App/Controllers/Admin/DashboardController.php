<?php 
namespace App\Controllers\Admin;

use System\Controller;


class DashboardController extends Controller
{
	public function index()
	{
		$view =  $this->view->render('main/dashboard');
	    return $this->adminLayout->render($view);
	} 

	public function submit()
	{
		
		return;
		$this->validator->required('email')->email('email')->unique('email',['users','email']);
		$this->validator->required('password')->minLen('password' , 8);
		$this->validator->match('password' ,'conf-password');

		$file = $this->request->file('image');
		if($file->isImage())
		{
			$file->moveTo($this->file->to('public/uploads/images'));
		}
		
	}

}
?>