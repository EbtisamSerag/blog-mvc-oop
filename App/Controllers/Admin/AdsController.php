<?php 
namespace App\Controllers\admin;

use System\Controller;
use System\Database;

class AdsController extends Controller
{
	/**
	*display Ads  list
	*@return mixed
	*/
	public function index()
	{
		$this->html->setTitle('Ads');
		$data['ads'] = $this->load->model('Ad')->all();

		$data['success'] = $this->session->has('success') ? $this->session->pull('success') :null;
		
		$view = $this->view->render('admin/ads/list',$data);
		return $this->adminLayout->render($view);
	}

	/**
	*open ads  form
	*@return string
	*/ 
	public function add()
	{
		return $this->form();
	}



	/**
	*submit for creating new ad
	*@return string|json
	*
	*/
	public function submit()
	{ 
		$json = [];
		if($this->isValid())
		{
			$this->load->model('Ad')->create();

		    $json['success'] = 'Ad Has Been Added Successfully';
		    $json['redirectTo'] = $this->url->link('admin/ads');


		}else{
			
			$json['errors'] = $this->validator->flattenMessages();
		}
		return json_encode($json);
	}

	/**
	*display edit form
	*@param int $id
	*@return string
	*/
	public function edit($id)
	{
		$adModel = $this->load->model('Ad');
        
		if(! $adModel->exists($id))
		{
			return $this->url->redirectTo('/404');
		}
		
		$ad = $adModel->get($id);
		

        return $this->form($ad);
		
		
 	}

 	/**
 	*display form
 	*/
 	private function form($ad = null)
 	{
 		if($ad)
 		{
 			//edit form 
 			$data['target'] = 'edit-ad-' .$ad->id;
 			$data['action'] = $this->url->link('/admin/ads/save/' . $ad->id);
 			$data['heading'] = 'Edit ' . $ad->name;
 		}else{
 			//add form
 			$data['target'] = 'add-ad-form';
 			$data['action'] = $this->url->link('/admin/ads/submit');
 			$data['heading'] = 'Add Ad ';
 		}
 		$ad = (array) $ad;
 		$data['name'] = array_get($ad ,'name');
 		$data['ad_url'] = array_get($ad ,'link');
 		$data['ad_page'] = array_get($ad ,'page');
 		$data['status'] = array_get($ad ,'status','enabled');

 		$data['start_at'] = ! empty($ad['start_at']) ?date('d-m-y',$ad['start_at']) : false;
 		$data['end_at'] =  ! empty($ad['end_at']) ?date('d-m-y',$ad['end_at']) : false;
 		$data['image'] = '';

 		if(!empty($ad['image']))
 		{
 			//default path to upload user image : public/uploads/images
 			$data['image'] = $this->url->link('Public/uploads/images/' . $ad['image']);
 		}
        $data['pages'] = $this->getPermissionPages();
 		return $this->view->render('admin/ads/form' ,$data);

 	} 


 	/**
	*submit for creating new user
	*@return string|json
	*
	*/
	public function save($id)
	{
        $json = [];
		if($this->isValid($id))
		{
			$this->load->model('Ad')->update($id);

		    $json['success'] = 'Ad Has Been Updated Successfully';
		    $json['redirectTo'] = $this->url->link('admin/ads');


		}else{
			
			$json['errors'] = $this->validator->flattenMessages();
		}
		return json_encode($json);
	}

	/**
	*delete record
	*@param in $id
	*@return mixed
	*/
	public function delete($id)
	{
		$adModel = $this->load->model('Ad');
        
		if(! $adModel->exists($id) OR $id == 1)
		{
			return $this->url->redirectTo('/404');
		}
		$adModel->delete($id);
		$this->session->set('success', 'Ad Has Been Deleted Successfully') ;
		return $this->url->redirectTo('/admin/ads');
	}

	/**
 	*get all permission pages
 	*@return array
 	*
 	*/
 	private function getPermissionPages()
 	{
 		$permissions = [];

 		foreach($this->route->routes() as $route)
 		{
 			if(strpos($route['url'], '/admin') !== 0)
 			{
 				$permissions[] = $route['url'];
 			}
 		}
 		return $permissions;
 	} 

	/**
	*validate the form
	*@param int $id
	*@return bool
	*/
	private function isValid($id = null)
	{
		$this->validator->required('name');
		$this->validator->required('ad_url');
		$this->validator->required('page');
		$this->validator->required('start_at');
		$this->validator->required('end_at');

        
        if(is_null($id))
        {
        	$this->validator->requiredFile('image')->image('image'); 
        	                                
        }else{
        	$this->validator->image('image');
        }
		return $this->validator->passes();
	}

}
?>