<?php 
namespace App\Controllers\admin;

use System\Controller;
use System\Database;

class SettingsController extends Controller
{
	/**
	*display settings  form
	*@return mixed
	*/
	public function index()
	{
		
		$this->html->setTitle('Settings');

		$data['success'] = $this->session->has('success') ? $this->session->pull('success') :null;
		$data['errors'] = $this->session->has('errors') ? $this->session->pull('errors') :null;
        
        $settingsModel = $this->load->model('Setting');
		$settingsModel->loadAll();
		
		$data['action'] = $this->url->link('/admin/settings/save');

		$data['site_name'] = $this->request->post('site_name')?:$settingsModel->get('site_name');
		$data['site_email'] = $this->request->post('site_email')?:$settingsModel->get('site_email');
		$data['site_status'] = $this->request->post('status')?:$settingsModel->get('status');
		$data['site_close_msg'] = $this->request->post('message')?:$settingsModel->get('message');

		
		$view = $this->view->render('admin/settings/form',$data);
		return $this->adminLayout->render($view);
	}


	/**
	*submit for creating new category
	*@return string|json
	*
	*/
	public function save()
	{
        
		if($this->isValid())
		{
			$this->load->model('Setting')->update();

		    $this->session->set('success', 'Setting Has Been Updated Successfully') ;
		    
		    return $this->url->redirectTo('/admin/settings');


		}else{
			
			$this->session->set('errors', $this->validator->getMessages()) ;
		}
		return $this->url->redirectTo('/admin/settings');
	}



	/**
	*validate the form
	*@param int $id
	*@return bool
	*/
	private function isValid($id = null)
	{
		$this->validator->required('site_name');
		$this->validator->required('site_email');
		$this->validator->required('status');

		return $this->validator->passes();
	}

}
?>