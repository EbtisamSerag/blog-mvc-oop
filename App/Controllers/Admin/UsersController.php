<?php 
namespace App\Controllers\admin;

use System\Controller;
use System\Database;

class UsersController extends Controller
{
	/**
	*display users  list
	*@return mixed
	*/
	public function index()
	{
		$this->html->setTitle('Users');
		$data['users'] = $this->load->model('User')->all();

		$data['success'] = $this->session->has('success') ? $this->session->pull('success') :null;
		
		$view = $this->view->render('admin/users/list',$data);
		return $this->adminLayout->render($view);
	}

	/**
	*open users  form
	*@return string
	*/ 
	public function add()
	{
		return $this->form();
	}



	/**
	*submit for creating new user
	*@return string|json
	*
	*/
	public function submit()
	{ 
		$json = [];
		if($this->isValid())
		{
			$this->load->model('User')->create();

		    $json['success'] = 'User Has Been Added Successfully';
		    $json['redirectTo'] = $this->url->link('admin/users');


		}else{
			
			$json['errors'] = $this->validator->flattenMessages();
		}
		return json_encode($json);
	}

	/**
	*display edit form
	*@param int $id
	*@return string
	*/
	public function edit($id)
	{
		$userModel = $this->load->model('User');
        
		if(! $userModel->exists($id))
		{
			return $this->url->redirectTo('/404');
		}
		
		$user = $userModel->get($id);
		

        return $this->form($user);
		
		
 	}

 	/**
 	*display form
 	*/
 	private function form($user = null)
 	{
 		if($user)
 		{
 			//edit form 
 			$data['target'] = 'edit-user-' .$user->id;
 			$data['action'] = $this->url->link('/admin/users/save/' . $user->id);
 			$data['heading'] = 'Edit ' . $user->f_name . ' ' .$user->l_name;
 		}else{
 			//add form
 			$data['target'] = 'add-user-form';
 			$data['action'] = $this->url->link('/admin/users/submit');
 			$data['heading'] = 'Add User ';
 		}
 		$user = (array) $user;
 		$data['f_name'] = array_get($user ,'f_name');
 		$data['l_name'] = array_get($user ,'l_name');
 		$data['status'] = array_get($user ,'status','enabled');
 		$data['users_group_id'] = array_get($user ,'users_group_id');
 		$data['email'] = array_get($user ,'email');
 		$data['image'] = array_get($user ,'image');
 		$data['gender'] = array_get($user ,'gender');

        
 		$data['birthday'] = '';
 		$data['image'] = '';

 		if(!empty($user['birthday']))
 		{
 			$data['birthday'] = date('d-m-y',$user['birthday']);
 		}

 		if(!empty($user['image']))
 		{
 			//default path to upload user image : public/uploads/images
 			$data['image'] = $this->url->link('Public/uploads/images/' . $user['image']);
 		}
 		$data['users_groups'] = $this->load->model('UsersGroup')->all();

 		return $this->view->render('admin/users/form' ,$data);

 	} 


 	/**
	*submit for creating new user
	*@return string|json
	*
	*/
	public function save($id)
	{
        $json = [];
		if($this->isValid($id))
		{
			$this->load->model('User')->update($id);

		    $json['success'] = 'User Has Been Updated Successfully';
		    $json['redirectTo'] = $this->url->link('admin/users');


		}else{
			
			$json['errors'] = $this->validator->flattenMessages();
		}
		return json_encode($json);
	}

	/**
	*delete record
	*@param in $id
	*@return mixed
	*/
	public function delete($id)
	{
		$userModel = $this->load->model('User');
        
		if(! $userModel->exists($id) OR $id == 1)
		{
			return $this->url->redirectTo('/404');
		}
		$userModel->delete($id);
		$this->session->set('success', 'User Has Been Deleted Successfully') ;
		return $this->url->redirectTo('/admin/users');
	}

	/**
	*validate the form
	*@param int $id
	*@return bool
	*/
	private function isValid($id = null)
	{
		$this->validator->required('f_name','First Name is Required');
		$this->validator->required('l_name','Last Name is Required');
		$this->validator->required('email')->email('email');
		$this->validator->unique('email',['user','email','id',$id]);

        
        if(is_null($id))
        {
        	/**
        	*if $id in null the this method is called  
        	*to create new user
        	*/
        	$this->validator->required('password')
        	                ->minlen('password',8)
        	                ->match('password','conf_password','Confirm Password should match with password');
        	$this->validator->requiredFile('image')->image('image'); 
        	                                
        }else{
        	$this->validator->image('image');
        }
		return $this->validator->passes();
	}

}
?>