<?php 
namespace App\Controllers\Admin;

use System\Controller;
use System\Database;

class CategoriesController extends Controller
{
	/**
	*display categories list
	*@return mixed
	*/
	public function index()
	{
		$this->html->setTitle('Categories');
		$data['categories'] = $this->load->model('Category')->all();

		$data['success'] = $this->session->has('success') ? $this->session->pull('success') :null;
		
		$view = $this->view->render('admin/categories/list',$data);
		return $this->adminLayout->render($view);
	}

	/**
	*open category form
	*@return string
	*/ 
	public function add()
	{
		$data['action'] = $this->url->link('/admin/categories/submit');
		return $this->view->render('admin/categories/form', $data);
	}



	/**
	*submit for creating new category
	*@return string|json
	*
	*/
	public function submit()
	{
		$json = [];
		if($this->isValid())
		{
			$this->load->model('Category')->create();

		    $json['success'] = 'Category Has Been Created Successfully';
		    $json['redirectTo'] = $this->url->link('admin/categories');


		}else{
			
			$json['errors'] = $this->validator->flattenMessages();
		}
		return json_encode($json);
	}

	/**
	*display edit form
	*@param int $id
	*@return string
	*/
	public function edit($id)
	{
		$categoryModel = $this->load->model('Category');
        
		if(! $categoryModel->exists($id))
		{
			return $this->url->redirectTo('/404');
		}
		
		$data['category'] = $categoryModel->get($id);
		$data['errors'] = $this->session->has('errors') ? $this->session->pull('errors') :null;

		$this->html->setTitle('Update ' . $data['category']->name);

		$view = $this->view->render('admin/categories/update_form' ,$data);
		return $this->adminLayout->render($view);
 	}

 	/**
	*submit for creating new category
	*@return string|json
	*
	*/
	public function save($id)
	{

		if($this->isValid())
		{
			$this->load->model('Category')->update($id);

		    $this->session->set('success', 'Category Has Been Updated Successfully') ;
		    return $this->url->redirectTo('/admin/categories');


		}else{
			
			$this->session->set('errors', $this->validator->getMessages()) ;
		}
		return $this->url->redirectTo('/admin/categories/edit/' .$id);
	}

	/**
	*delete record
	*@param in $id
	*@return mixed
	*/
	public function delete($id)
	{
		$categoryModel = $this->load->model('Category');
        
		if(! $categoryModel->exists($id))
		{
			return $this->url->redirectTo('/404');
		}
		$categoryModel->delete($id);
		$this->session->set('success', 'Category Has Been Deleted Successfully') ;
		return $this->url->redirectTo('/admin/categories');
	}

	/**
	*validate the form
	*@return bool
	*/
	private function isValid()
	{
		$this->validator->required('name','Category Name is Required');
		return $this->validator->passes();
	}

}
?>