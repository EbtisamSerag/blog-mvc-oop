<?php 
namespace App\Controllers\admin;

use System\Controller;
use System\Database;

class UsersGroupsController extends Controller
{
	/**
	*display users groups list
	*@return mixed
	*/
	public function index()
	{
		$this->html->setTitle('Users Groups');
		$data['users_groups'] = $this->load->model('UsersGroup')->all();

		$data['success'] = $this->session->has('success') ? $this->session->pull('success') :null;
		
		$view = $this->view->render('admin/users_groups/list',$data);
		return $this->adminLayout->render($view);
	}

	/**
	*open users groups form
	*@return string
	*/ 
	public function add()
	{
		return $this->form();
	}



	/**
	*submit for creating new category
	*@return string|json
	*
	*/
	public function submit()
	{
		$json = [];
		if($this->isValid())
		{
			$this->load->model('UsersGroup')->create();

		    $json['success'] = 'Users Groups Has Been Created Successfully';
		    $json['redirectTo'] = $this->url->link('admin/users_groups');


		}else{
			
			$json['errors'] = $this->validator->flattenMessages();
		}
		return json_encode($json);
	}

	/**
	*display edit form
	*@param int $id
	*@return string
	*/
	public function edit($id)
	{
		$usersGroupsModel = $this->load->model('UsersGroup');
        
		if(! $usersGroupsModel->exists($id))
		{
			return $this->url->redirectTo('/404');
		}
		
		$users_groups = $usersGroupsModel->get($id);
		

        return $this->form($users_groups);
		
		
 	}

 	/**
 	*display form
 	*/
 	private function form($users_groups = null)
 	{
 		if($users_groups)
 		{
 			//edit form 
 			$data['target'] = 'edit-users-groups-' .$users_groups->id;
 			$data['action'] = $this->url->link('/admin/users_groups/save/' . $users_groups->id);
 			$data['heading'] = 'Edit ' . $users_groups->name;
 		}else{
 			//add form
 			$data['target'] = 'add-users-groups-form';
 			$data['action'] = $this->url->link('/admin/users_groups/submit');
 			$data['heading'] = 'Add Users Groups';
 		}
 		$data['name'] = $users_groups ?$users_groups->name :null;
 		$data['users_group_pages'] = $users_groups ?$users_groups->pages :[];

 		//$data['status'] = $users_groups ?$user_group->status :'enabled';
 		$data['pages'] = $this->getPermissionPages();
 		return $this->view->render('admin/users_groups/form' ,$data);

 	} 

 	/**
 	*get all permission pages
 	*@return array
 	*
 	*/
 	private function getPermissionPages()
 	{
 		$permissions = [];

 		foreach($this->route->routes() as $route)
 		{
 			if(strpos($route['url'], '/admin') === 0)
 			{
 				$permissions[] = $route['url'];
 			}
 		}
 		return $permissions;
 	} 

 	/**
	*submit for creating new users groups
	*@return string|json
	*
	*/
	public function save($id)
	{
        $json = [];
		if($this->isValid())
		{
			$this->load->model('UsersGroup')->update($id);

		    $json['success'] = 'Users Groups Has Been Updated Successfully';
		    $json['redirectTo'] = $this->url->link('admin/users_groups');


		}else{
			
			$json['errors'] = $this->validator->flattenMessages();
		}
		return json_encode($json);
	}

	/**
	*delete record
	*@param in $id
	*@return mixed
	*/
	public function delete($id)
	{
		$usersGroupsModel = $this->load->model('UsersGroup');
        
		if(! $usersGroupsModel->exists($id) OR $id == 1)
		{
			return $this->url->redirectTo('/404');
		}
		$usersGroupsModel->delete($id);
		$this->session->set('success', 'Users Groups Has Been Deleted Successfully') ;
		return $this->url->redirectTo('/admin/users_groups');
	}

	/**
	*validate the form
	*@return bool
	*/
	private function isValid()
	{
		$this->validator->required('name','Users Groups Name is Required');
		return $this->validator->passes();
	}

}
?>