<?php 
namespace App\Controllers\Blog;

use System\Controller;
use System\Database;

class HomeController extends Controller
{
	public function index()
	{
		$data['posts'] = $this->load->model('Post')->latest();

		$this->html->setTitle($this->settings->get('site_name'));

		$postController = $this->load->controller('Blog/post');
		$data['post_box'] = function ($post) use ($postController){
			return $postController->box($post);
		};
		$view =  $this->view->render('blog/home',$data);
		
	    return $this->blogLayout->render($view);
	} 

}
?>