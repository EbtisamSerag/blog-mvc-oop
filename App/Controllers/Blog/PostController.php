<?php 
namespace App\Controllers\Blog;

use System\Controller;
use System\Database;

class PostController extends Controller
{
	/**
	*display post page
	*@param strin $title
	*@param id $id
	*@return mixed
	*/
	public function index($title, $id)
	{
		$post = $this->load->model('Post')->getPostWithComments($id);
		if(!$post)
		{
			return $this->url->redirectTo('/404');
		}

		$this->html->setTitle($this->settings->get($post->title));
		$data['post'] = $post;
		
		$view =  $this->view->render('blog/post',$data);
		
	    return $this->blogLayout->render($view);
	} 

	/**
	*load the post box view from the given post
	*@param \stdClass $post
	*@return string
	*/
	public function box($post)
	{
		return $this->view->render('blog/post-box',compact('post'));
	}
 
	/**
	*add new comment to the given post
	*@param string $title
	*@param int $id
	*@return mixed
	*/
	public function addComment($title,$id)
	{
		$comment = $this->request->post('comment');
		$postModel = $this->load->model('Post');
		$loginModel = $this->load->model('Login');

		$post = $postModel->get($id);
		if(!$post OR $post->status == 'disabled' OR !$comment OR !$loginModel->isLogged())
		{
			return $this->url->redirectTo('/404');
		}

		$user = $loginModel->user();

		$postModel->addNewComment($id,$comment,$user->id);
		return $this->url->redirectTo('/post/' . $title . '/' . $id . '#comments');
	}

}
?>