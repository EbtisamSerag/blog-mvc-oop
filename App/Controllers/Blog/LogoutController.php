<?php
namespace App\Controllers\Blog;
use System\Controller;

class LogoutController extends Controller
{
	/**
	*log user out
	*@return void
	*/
	public function index()
	{
        $this->session->destroy();
        $this->cookie->destroy();

        return $this->url->redirectTo('/login');
    }

    /**
    *submit login form 
    *@return mixed
    */
    public function submit()
    {
    	if($this->isValid())
        {
        	$loginModel = $this->load->model('Login');
        	$loginUser = $loginModel->user();
        	if($this->request->post('remember'))
        	{
        		//save data in cookies
        		$this->cookie->set('login' ,$loginUser->code);
        	}else{
        		//save data in session 
                $this->session->set('login' ,$loginUser->code);
        	}
        	
        	$json = [];
        	$json['success'] = 'Welcome '.$loginUser->f_name;
        	$json['redirect'] = $this->url->link('/');
            
        	return json_encode($json);
        }else{
        	
        	$json = [];
        	$json['errors'] = implode('<br>', $this->errors);
        	return json_encode($json);
        }
    }
    /**
    *validate login form 
    *@return bool
    */
    private function isValid()
    {
    	$email = $this->request->post('email');
    	$password = $this->request->post('password');

    	if(! $email)
    	{
    		$this->errors[] = 'Pleaze insert email';
    	}elseif(! filter_var($email, FILTER_VALIDATE_EMAIL)){
    		$this->errors[] = 'Pleaze insert valid email';
    	}

    	if(! $password)
    	{
    		$this->errors[] = 'Pleaze insert password';
    	}
    	if(! $this->errors)
    	{
    		$loginModel = $this->load->model('Login');
	    	if(! $loginModel->isValidLogin($email, $password))
	    	{
	    		$this->errors[] = 'invalid login data';
	    	}
    	}
    	//return true or false
    	return empty($this->errors);
    	
    }		
}
?>