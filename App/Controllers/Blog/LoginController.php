<?php
namespace App\Controllers\Blog;
use System\Controller;

class LoginController extends Controller
{
	/**
	*display login form 
	*@return mixed
	*/
	public function index()
	{
        $this->blogLayout->title('Login');
		$loginModel = $this->load->model('Login');

        //disale sidebare
        $this->blogLayout->disabled('sidebar');

		if($loginModel->isLogged())
		{
			$this->url->redirectTo('/');
		}
		$data['errors'] = $this->errors;

		$view =  $this->view->render('blog/users/login');
        return $this->blogLayout->render($view);
    }

    /**
    *submit login form 
    *@return mixed
    */
    public function submit()
    {
    	if($this->isValid())
        {
        	$loginModel = $this->load->model('Login');
        	$loginUser = $loginModel->user();
        	if($this->request->post('remember'))
        	{
        		//save data in cookies
        		$this->cookie->set('login' ,$loginUser->code);
        	}else{
        		//save data in session 
                $this->session->set('login' ,$loginUser->code);
        	}
        	
        	$json = [];
        	$json['success'] = 'Welcome '.$loginUser->f_name;
        	$json['redirectTo'] = $this->url->link('/');
            
        	return json_encode($json);
        }else{
        	
        	$json = [];
        	$json['errors'] = implode('<br>', $this->errors);
        	return json_encode($json);
        }
    }
    /**
    *validate login form 
    *@return bool
    */
    private function isValid()
    {
    	$email = $this->request->post('email');
    	$password = $this->request->post('password');

    	if(! $email)
    	{
    		$this->errors[] = 'Pleaze insert email';
    	}elseif(! filter_var($email, FILTER_VALIDATE_EMAIL)){
    		$this->errors[] = 'Pleaze insert valid email';
    	}

    	if(! $password)
    	{
    		$this->errors[] = 'Pleaze insert password';
    	}
    	if(! $this->errors)
    	{
    		$loginModel = $this->load->model('Login');
	    	if(! $loginModel->isValidLogin($email, $password))
	    	{
	    		$this->errors[] = 'invalid login data';
	    	}
    	}
    	//return true or false
    	return empty($this->errors);
    	
    }		
}
?>