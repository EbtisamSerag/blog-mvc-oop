<?php 
namespace App\Controllers\Blog\Common;

use System\Controller;
use System\View\ViewInterface;

class LayoutController extends Controller
{
    /**
    *disabled sections container
    *@var array
    */
    private $disabledSections = [];

	/**
	*render the layout with the given view object
	*@param \System\View\VieInterface $view
	*/
	public function render(ViewInterface $view)
	{
		$data['content'] = $view;
        
        $sections = ['header','sidebar','footer'];

        foreach($sections as $section)
        {
        	$data[$section]  = in_array($section, $this->disabledSections) ? '' :$this->load->controller('Blog/Common/'. ucfirst($section))->index();
        }

		return $this->view->render('blog/common/layout', $data);
		
	} 
    
    /**
    *determine what will be not disabled in the layout page 
    *@param string
    *@return $this
    */
    public function disabled($section)
    {
    	$this->disabledSections[] = $section;
    	return $this; 
    }

    /**
    *set the title for the blog page
    *@param string $title
    *@return void
    */
    public function title($title)
    {
         $this->html->setTitle( $title.' | '. $this->settings->get('site_name'));
    }
}
?>