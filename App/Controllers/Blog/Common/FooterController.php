<?php 
namespace App\Controllers\Blog\Common;

use System\Controller;

class FooterController extends Controller
{
	public function index()
	{
		return $this->view->render('blog/common/footer');
	} 

}
?>