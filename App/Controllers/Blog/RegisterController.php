<?php 
namespace App\Controllers\Blog;

use System\Controller;
use System\Database;

class RegisterController extends Controller
{
	public function index()
	{
		$loginModel = $this->load->model('Login');
		if($loginModel->isLogged())
		{
			$this->url->redirectTo('/');
		}
		$this->blogLayout->title('Create New Account');

		//disale sidebare
		$this->blogLayout->disabled('sidebar');

		$view =  $this->view->render('blog/users/register');
	    return $this->blogLayout->render($view);
	} 

    /**
	*submit for creating new user
	*@return string|json
	*
	*/
	public function submit()
	{ 
		$json = [];
		if($this->isValid())
		{
			$this->request->setPost('users_group_id',3);
			$this->load->model('User')->create();

		    $json['success'] = 'User Has Been Added Successfully';
		    $json['redirectTo'] = $this->url->link('/');


		}else{
			
			$json['errors'] = $this->validator->flattenMessages();
		}
		return json_encode($json);
	}


	/**
	*validate the form
	*@param int $id
	*@return bool
	*/
	private function isValid()
	{
		$this->validator->required('f_name','First Name is Required');
		$this->validator->required('l_name','Last Name is Required');
		$this->validator->required('email')->email('email');
		$this->validator->unique('email',['users','email']);
        $this->validator->required('password')->minlen('password',4)
        	            ->match('password','conf_password','Confirm Password should match with password');
        $this->validator->requiredFile('image')->image('image');
		return $this->validator->passes();
	}

}
?>