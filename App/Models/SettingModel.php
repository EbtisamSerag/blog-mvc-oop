<?php
namespace App\Models;
use System\Model;

class SettingModel extends Model
{   
    protected $table = 'settings';
    
    private $settings = [];

    /**
    *load and store all settings in the database
    *
    *@return void
    */
    public function loadAll()
    {
        foreach($this->all() as $setting)
        {
            $this->settings[$setting->s_key] = $setting->s_value;
        }
    }

    /**
    *get setting by key
    *@param string $key
    *@return mixed
    */
    public function get($key)
    {
        return array_get($this->settings, $key);
    }
    


    /**
    *update  user record by id
    *@param int $id
    *@return void
    *
    */
    public function update()
    {
        $keys = ['site_name','site_email','status','message'];

        foreach($keys as $key)
        {
            $this->where('s_key =?',$key)->delete($this->table);

            $this->data('s_key', $key)
             ->data('s_value', $this->request->post($key))
             ->insert($this->table);
        }
    }
}
?>