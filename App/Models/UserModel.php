<?php
namespace App\Models;
use System\Model;

class UserModel extends Model
{   
    protected $table = 'users';
    

    /**
    *get all users
    *
    */
    public function all()
    {
        return $this->select('u.*','ug.name AS user_group')
                       ->from('users u')
                       ->join('LEFT JOIN users_groups ug ON u.users_group_id = ug.id')
                       ->fetchAll();
    }
    
    /**
    *upload user image
    *@return string
    */
    private function uploadImage()
    {
        $image = $this->request->file('image');
        if(! $image->exists())
        {
            return '';
        }
        return $image->moveTo($this->app->file->toPublic('uploads/images'));
    }


    /**
    *create new user
    *
    */
    public function create()
    {
        
        $image = $this->uploadImage();

        if($image)
        {
            $this->data('image',$image);
        }
        $this->data('f_name', $this->request->post('f_name'))
             ->data('l_name', $this->request->post('l_name'))
             ->data('users_group_id', $this->request->post('users_group_id'))
             ->data('email', $this->request->post('email'))
             ->data('password', password_hash($this->request->post('password'), PASSWORD_DEFAULT))
             ->data('status', $this->request->post('status'))
             ->data('gender', $this->request->post('gender'))
             ->data('birthday', strtotime($this->request->post('birthday')))
             ->data('ip', $this->request->server('REMOTE_ADDR'))
             ->data('created_at', $now = time())
             ->data('code', sha1($now . mt_rand()))
             ->insert($this->table);   
    }

    /**
    *update  user record by id
    *@param int $id
    *@param int $usersGroupId
    *@return void
    *
    */
    public function update($id ,$usersGroupId = null)
    {
        $image = $this->uploadImage();

        if($image)
        {
            $this->data('image',$image);
        }
        $password = $this->request->post('password');
        if($password)
        {
            $this->data('password', password_hash($password, PASSWORD_DEFAULT));
        }
        if(is_null($usersGroupId))
        {
            $usersGroupId = $this->request->post('users_group_id');
        }
        $this->data('f_name', $this->request->post('f_name'))
             ->data('l_name', $this->request->post('l_name'))
             ->data('users_group_id',$usersGroupId)
             ->data('email', $this->request->post('email'))
             ->data('status', $this->request->post('status'))
             ->data('gender', $this->request->post('gender'))
             ->data('birthday', strtotime($this->request->post('birthday')))
             ->where('id =?',$id)
             ->update($this->table);
    }
}
?>