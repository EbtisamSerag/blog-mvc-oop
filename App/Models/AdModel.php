<?php
namespace App\Models;
use System\Model;

class AdModel extends Model
{   
	protected $table = 'ads';
    

    
    /**
    *upload user image
    *@return string
    */
    private function uploadImage()
    {
        $image = $this->request->file('image');
        if(! $image->exists())
        {
            return '';
        }
        return $image->moveTo($this->app->file->toPublic('uploads/images'));
    }

    /**
    *get enabled ads for current route
    *@return array    
    */
    public function enabled()
    {
        $now = time();
        $currentRoutes = $this->route->getCurrentRouteUrl();
        return $this->where('status=? AND page=? AND start_at <=? AND end_at >=?','enabled',$currentRoutes,$now,$now)->fetchAll($this->table);
    }
     

    /**
    *create new user
    *
    */
    public function create()
    {
        
        $image = $this->uploadImage();

        if($image)
        {
            $this->data('image',$image);
        }
        $this->data('link', $this->request->post('ad_url'))
             ->data('name', $this->request->post('name'))
             ->data('status', $this->request->post('status'))
             ->data('page', $this->request->post('page'))
             ->data('start_at',strtotime($this->request->post('start_at')))
             ->data('end_at',strtotime($this->request->post('end_at')))
             ->insert($this->table);   
    }

    /**
    *update  user record by id
    *@param int $id
    *@return void
    *
    */
    public function update($id)
    {
        
        $image = $this->uploadImage();

        if($image)
        {
            $this->data('image',$image);
        }
        $this->data('link', $this->request->post('ad_url'))
             ->data('name', $this->request->post('name'))
             ->data('status', $this->request->post('status'))
             ->data('page', $this->request->post('page'))
             ->data('start_at',strtotime($this->request->post('start_at')))
             ->data('end_at',strtotime($this->request->post('end_at')))
             ->where('id =?',$id)
             ->update($this->table);
    }
}
?>