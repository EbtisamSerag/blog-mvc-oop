<?php
namespace App\Models;
use System\Model;

class UsersGroupModel extends Model
{   
	/**
	*table name 
	*@var string
	*/
	protected $table = 'users_groups';
    

    /**
    *get users groups
    *@return mixed
    */
     public function get($id)
     {
        $usersGroup = Parent::get($id);
        pre($usersGroup);
        if($usersGroup)
            {
                $pages = $this->select('page')->where('users_group_id = ?',$usersGroup->id)->fetchAll('permissions');
                $usersGroup->pages = [];
                if($pages)
                {
                    foreach($pages as $page)
                    {
                        $usersGroup->pages[] = $page->page;
                    }
                }
            }
        return $usersGroup;
     }

    /**
    *create new users groups record
    *@return void
    *
    */
    public function create()
    {

    	$usersGroupId = $this->data('name',$this->request->post('name'))
    	                   //->data('status',$this->request->post('status'))
    	                     ->insert($this->table)->lastInsertId();
        //remove an empty values in the array
        $pages = array_filter($this->request->post('pages'));

        foreach($pages as $page)
        {
            $this->data('users_group_id',$usersGroupId)
                 ->data('page',$page)
                 ->insert('permissions');
        }                     
    }

    /**
    *update  users groups record by id
    *@param int $id
    *@return void
    *
    */
    public function update($id)
    {
        $this->data('name',$this->request->post('name'))
            // ->data('status',$this->request->post('status'))
             ->where('id =?' ,$id)
             ->update($this->table);

        //delete all permissions from user before add new one
        $this->where('users_group_id = ?' ,$id)->delete('permissions');

        //remove an empty values in the array
        $pages = array_filter($this->request->post('pages'));
        
        foreach($pages as $page)
        {
            $this->data('users_group_id',$id)
                 ->data('page',$page)
                 ->insert('permissions');
        }      
    }
	

}
?>