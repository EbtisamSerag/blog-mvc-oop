<div class="modal fade" id="<?php echo $target?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel"><?php echo $heading?></h4>
            </div>
            <div class="modal-body">
                <form action="<?php echo $action;?>" class="form-modal form">
                  <div id="form-results"></div>

                  <div class="form-group col-sm-6">
                    <label for="ad_url">Ad Url</label>
                    <input type="text" class="form-control" id="ad_url" 
                   value="<?php echo $ad_url ?>" name="ad_url" placeholder="Ad Url">
                  </div>
                  <div class="form-group col-sm-6">
                    <label for="name">Ad Name</label>
                    <input type="text" class="form-control" id="name" 
                   value="<?php echo $name ?>" name="name" placeholder="Ad Name ">
                  </div>

                  <div class="form-group col-sm-6">
                    <label for="start_at">Start at</label>
                    <input type="text" class="form-control" id="start_at" 
                   value="<?php echo $start_at ?>" name="start_at" placeholder="Startting AD date">
                  </div>

                  <div class="form-group col-sm-6">
                    <label for="end_at">End at</label>
                    <input type="text" class="form-control" id="end_at" 
                   value="<?php echo $end_at ?>" name="end_at" placeholder="Ending AD date">
                  </div>

                   <div class="form-group col-sm-6">
                    <label for="status">Status</label>
                    <select class="form-control" name="status" id="status">
                        <option value="enabled" <?php echo $status == 'enabled' ?'selected' : false?>>Enabled </option>
                        <option value="disabled" <?php echo $status == 'disabled' ?'selected' : false?>>Disabled</option>
                    </select>
                </div>


                  <div class="form-group col-sm-6">
                    <label for="page">Page</label>
                    <select class="form-control" name="page" id="page" >
                      <?php foreach($pages as $page){?>
                        <option value="<?php echo $page?>" <?php echo $page == $ad_page? 'selected' :false?>><?php echo $page?> </option>
                      <?php }?>
                        
                    </select>
                  </div>


                  <div class="clearfix"></div>
                   <div class="form-group col-sm-6">
                    <label for="image">Image</label>
                    <input type="file" class="form-control" id="image" 
                    name="image">
                  </div>

                  <?php if($image) {?>
                    <div class="form-group col-sm-6">
                    <img src="<?php echo $image?>" alt="" style="width: 100px;height: 50px;">
                  </div>
                  <?php }?>
                  <div class="clearfix"></div>
                  <button class="btn btn-info submit-btn">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>