  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo url('/admin')?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Ads</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
          <div class="col-sm-12">
              <div class="box" id="users-list">
                <div class="box-header with-border">
                  <h3 class="box-title">Manage  Ads </h3>
                  <button class="btn btn-danger pull-right open-popup" type="button" data-target="<?php echo url('admin/ads/add')?>" data-modal-target="#add-ad-form">Add New Ad</button>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                                   
                  <table class="table table-bordered">
                    <tr>
                        <th>#</th>
                        <th>Link</th>
                        <th>Name</th>
                        <th>image</th>
                        <th>page</th>
                        <th>Status</th>
                        <th>Start At</th>
                        <th>End At</th>
                        <th>Action</th>

                    </tr>
                    <?php foreach($ads as $ad) {?>
                    <tr>
                      <td><?php echo $ad->id?></td>
                      <td><?php echo $ad->link?></td>
                      <td><?php echo $ad->name ?></td>
                      <td><img src="<?php echo assets('uploads/images/' .$ad->image)?>" style="width: 100px;height: 50px"></td>
                      <td><?php echo $ad->page?></td>
                      <td><?php echo ucfirst($ad->status)?></td>
                      <td><?php echo date('d-m-y' ,$ad->start_at)?></td>
                      <td><?php echo date('d-m-y' ,$ad->end_at)?></td>

                      <td><button class="btn btn-info open-popup" type="button" data-target="<?php echo url('admin/ads/edit/' . $ad->id)?>" data-modal-target="#edit-ad-<?php echo $ad->id?>">Edit  <span class="fa fa-edit"></span></button>
                        <a href="<?php echo url('admin/ads/delete/' . $ad->id)?>" class="btn btn-danger delete" >Delete   <span class="fa fa-trash"></a>
                        </td>

                    </tr>
                  <?php }?>
                    
                  </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                  
                </div>
              </div>
          </div>
      </div>
      

    </section>