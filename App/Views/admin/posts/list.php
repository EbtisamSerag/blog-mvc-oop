  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo url('/admin')?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Posts</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
          <div class="col-sm-12">
              <div class="box" id="users-list">
                <div class="box-header with-border">
                  <h3 class="box-title">Manage  Posts </h3>
                  <button class="btn btn-danger pull-right open-popup" type="button" data-target="<?php echo url('admin/posts/add')?>" data-modal-target="#add-post-form">Add New Post</button>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                                   
                  <table class="table table-bordered">
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Category</th>
                        <th>Status</th>
                        <th>created_at</th>
                        <th>Action</th>

                    </tr>
                    <?php foreach($posts as $post) {?>
                    <tr>
                      <td><?php echo $post->id?></td>
                      <td><?php echo $post->title?></td>
                      <td><img src="<?php echo assets('uploads/images/' .$post->image)?>" style="width: 100px;height: 50px"></td>
                      <td><?php echo $post->category?></td>
                      <td><?php echo ucfirst($post->status)?></td>
                      <td><?php echo date('d-m-y')?></td>

                      <td><button class="btn btn-info open-popup" type="button" data-target="<?php echo url('admin/posts/edit/' . $post->id)?>" data-modal-target="#edit-post-<?php echo $post->id?>">Edit  <span class="fa fa-edit"></span></button>
                        <a href="<?php echo url('admin/posts/delete/' . $post->id)?>" class="btn btn-danger delete" >Delete   <span class="fa fa-trash"></a>
                        </td>

                    </tr>
                  <?php }?>
                    
                  </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                  
                </div>
              </div>
          </div>
      </div>
      

    </section>