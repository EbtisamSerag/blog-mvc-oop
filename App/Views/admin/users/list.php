  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo url('/admin')?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Users</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
          <div class="col-sm-12">
              <div class="box" id="users-list">
                <div class="box-header with-border">
                  <h3 class="box-title">Manage  Users </h3>
                  <button class="btn btn-danger pull-right open-popup" type="button" data-target="<?php echo url('admin/users/add')?>" data-modal-target="#add-user-form">Add New User</button>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                                   
                  <table class="table table-bordered">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Group</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Joined</th>
                        <th>Action</th>

                    </tr>
                    <?php foreach($users as $user) {?>
                    <tr>
                      <td><?php echo $user->id?></td>
                      <td><?php echo $user->f_name ." ".$user->l_name?></td>
                      <td><img src="<?php echo assets('uploads/images/' .$user->image)?>" style="width: 100px;height: 50px"></td>
                      <td><?php echo $user->user_group?></td>
                      <td><?php echo $user->email?></td>
                      <td><?php echo ucfirst($user->status)?></td>
                      <td><?php echo date('d-m-y')?></td>

                      <td><button class="btn btn-info open-popup" type="button" data-target="<?php echo url('admin/users/edit/' . $user->id)?>" data-modal-target="#edit-user-<?php echo $user->id?>">Edit  <span class="fa fa-edit"></span></button>
                        <?php if($user->id != 1) {?>
                        <a href="<?php echo url('admin/users/delete/' . $user->id)?>" class="btn btn-danger delete" >Delete   <span class="fa fa-trash"></a>
                        <?php }?>
                        </td>

                    </tr>
                  <?php }?>
                    
                  </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                  
                </div>
              </div>
          </div>
      </div>
      

    </section>