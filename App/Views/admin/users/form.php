<div class="modal fade" id="<?php echo $target?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel"><?php echo $heading?></h4>
            </div>
            <div class="modal-body">
                <form action="<?php echo $action;?>" class="form-modal form">
                  <div id="form-results"></div>
                  <div class="form-group col-sm-6">
                    <label for="f_name">First name</label>
                    <input type="text" class="form-control" id="f_name" 
                   value="<?php echo $f_name ?>" name="f_name" placeholder="First name">
                  </div>
                  <div class="form-group col-sm-6">
                    <label for="l_name">Last name</label>
                    <input type="text" class="form-control" id="l_name" 
                   value="<?php echo $l_name ?>" name="l_name" placeholder="last name">
                  </div>

                  <div class="form-group col-sm-6">
                    <label for="users_group_id">Group</label>
                    <select id="users_group_id"  class="form-control" name="users_group_id">
                    <?php foreach($users_groups as $users_group) {?>
                       <option value="<?php echo $users_group->id?>" <?php echo $users_group->id == $users_group_id ? 'selected' : false?>><?php echo $users_group->name?></option>
                    <?php }?>
                    </select>
                    
                  </div>

                  <div class="form-group col-sm-6">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" 
                   value="<?php echo $email ?>" name="email" placeholder="Email">
                  </div>

                   <div class="form-group col-sm-6">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" 
                    name="password" placeholder="Password">
                  </div>

                  <div class="form-group col-sm-6">
                    <label for="conf_password">Confirm Password</label>
                    <input type="password" class="form-control" id="conf_password" 
                     name="conf_password" placeholder="Confirm Password">
                  </div>

                <div class="form-group col-sm-6">
                    <label for="status">Status</label>
                    <select class="form-control" name="status" id="status">
                        <option value="enabled" <?php echo $status == 'enabled' ?'selected' : false?>>Enabled </option>
                        <option value="disabled" <?php echo $status == 'disabled' ?'selected' : false?>>Disabled</option>
                    </select>
                </div>

                <div class="form-group col-sm-6">
                    <label for="gender">Gender</label>
                    <select class="form-control" name="gender" id="gender">
                        <option value="male" <?php echo $gender == 'male' ?'selected' : false?>>Male </option>
                        <option value="female" <?php echo $gender == 'female' ?'selected' : false?>>Female</option>
                    </select>
                </div>

                <div class="form-group col-sm-6">
                    <label for="birthday">Birthday</label>
                    <input type="text" class="form-control" id="birthday" 
                    name="birthday" value="<?php echo $birthday ?>" placeholder="Birthday">
                  </div>
                  <div class="clearfix"></div>
                   <div class="form-group col-sm-6">
                    <label for="image">Image</label>
                    <input type="file" class="form-control" id="image" 
                    name="image">
                  </div>

                  <?php if($image) {?>
                    <div class="form-group col-sm-6">
                    <img src="<?php echo $image?>" alt="" style="width: 100px;height: 50px;">
                  </div>
                  <?php }?>
                  <div class="clearfix"></div>
                  <button class="btn btn-info submit-btn">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>