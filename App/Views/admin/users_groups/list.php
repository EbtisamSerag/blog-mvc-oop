  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo url('/admin')?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Users Groups</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
          <div class="col-sm-12">
              <div class="box" id="users-list">
                <div class="box-header with-border">
                  <h3 class="box-title">Manage Your Users Groups</h3>
                  <button class="btn btn-danger pull-right open-popup" type="button" data-target="<?php echo url('admin/users_groups/add')?>" data-modal-target="#add-users-groups-form">Add New Users Groups</button>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                                   
                  <table class="table table-bordered">
                    <tr>
                        <th>#</th>
                        <th>Group Name</th> 
                        <th>Action</th>

                    </tr>
                    <?php foreach($users_groups as $users_group) {?>
                    <tr>
                      <td><?php echo $users_group->id?></td>
                      <td><?php echo $users_group->name?></td>
                     
                      <td><button class="btn btn-info open-popup" type="button" data-target="<?php echo url('admin/users_groups/edit/' . $users_group->id)?>" data-modal-target="#edit-users-groups-<?php echo $users_group->id?>">Edit  <span class="fa fa-edit"></span></button>
                        <?php if($users_group->id != 1) {?>
                        <a href="<?php echo url('admin/users_groups/delete/' . $users_group->id)?>" class="btn btn-danger delete" >Delete   <span class="fa fa-trash"></a>
                        <?php }?>
                        </td>

                    </tr>
                  <?php }?>
                    
                  </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                  
                </div>
              </div>
          </div>
      </div>
      

    </section>