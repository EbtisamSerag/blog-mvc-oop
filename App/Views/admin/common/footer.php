
<!-- /.content -->
  </div>
<!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2016 <a href="http://www.aymanelash.com">Ayman Elash</a>.</strong> All rights
    reserved.
  </footer>
<!-- ./wrapper -->

<div class="modal fade" id="user-profile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Update User Profile</h4>
            </div>
            <div class="modal-body">
                <form action="<?php echo url('/admin/profile/update');?>" class="form-modal form">
                  <div id="form-results"></div>
                  <div class="form-group col-sm-6">
                    <label for="f_name">First name</label>
                    <input type="text" class="form-control" id="f_name" 
                   value="<?php echo $user->f_name ?>" name="f_name" placeholder="First name">
                  </div>
                  <div class="form-group col-sm-6">
                    <label for="l_name">Last name</label>
                    <input type="text" class="form-control" id="l_name" 
                   value="<?php echo $user->l_name ?>" name="l_name" placeholder="last name">
                  </div>

                  <div class="form-group col-sm-6">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" 
                   value="<?php echo $user->email ?>" name="email" placeholder="Email">
                  </div>

                   <div class="form-group col-sm-6">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" 
                    name="password" placeholder="Password">
                  </div>

                  <div class="form-group col-sm-6">
                    <label for="conf_password">Confirm Password</label>
                    <input type="password" class="form-control" id="conf_password" 
                     name="conf_password" placeholder="Confirm Password">
                  </div>

                <div class="form-group col-sm-6">
                    <label for="birthday">Birthday</label>
                    <input type="text" class="form-control" id="birthday" 
                    name="birthday" value="<?php echo date('d-m-y',$user->birthday) ?>" placeholder="Birthday">
                  </div>
                  <div class="clearfix"></div>
                   <div class="form-group col-sm-6">
                    <label for="image">Image</label>
                    <input type="file" class="form-control" id="image" 
                    name="image">
                  </div>

                  <?php if($user->image) {?>
                    <div class="form-group col-sm-6">
                    <img src="<?php echo assets('uploads/images/' .$user->image)?>" alt="" style="width: 100px;height: 50px;">
                  </div>
                  <?php }?>
                  <div class="clearfix"></div>
                  <button class="btn btn-info submit-btn">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

<!-- jQuery 2.2.0 -->
<script src="<?php echo assets('admin/plugins/jQuery/jQuery-2.2.0.min.js') ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo assets('admin/bootstrap/js/bootstrap.min.js') ?>"></script>
<!-- ckeditor-->
<script src="<?php echo assets('admin/ckeditor/ckeditor.js') ?>"></script>

<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo assets('admin/plugins/morris/morris.min.js') ?>"></script>
<!-- Sparkline -->
<script src="<?php echo assets('admin/plugins/sparkline/jquery.sparkline.min.js') ?>"></script>
<!-- jvectormap -->
<script src="<?php echo assets('admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>"></script>
<script src="<?php echo assets('admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo assets('admin/plugins/knob/jquery.knob.js') ?>"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo assets('admin/plugins/daterangepicker/daterangepicker.js') ?>"></script>
<!-- datepicker -->
<script src="<?php echo assets('admin/plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo assets('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>"></script>
<!-- Slimscroll -->
<script src="<?php echo assets('admin/plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo assets('admin/plugins/fastclick/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo assets('admin/dist/js/app.min.js') ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo assets('admin/dist/js/pages/dashboard.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo assets('admin/dist/js/demo.js') ?>"></script>


<script type="text/javascript">

	 CKEDITOR.replaceAll('message');
     //steps to sit activw class on sidebar links
     var currenturl = window.location.href;
     //get the last segment of url
     var segment = currenturl.split('/').pop();
     
     $('#' + segment + '-link').addClass('active');     
     
      
         //popup
	$('.open-popup').on('click', function(){
		btn = $(this);
		url = btn.data('target');
		modalTarget = btn.data('modal-target');

		//remove the target from page
		$(modalTarget).remove();
		// if($(modalTarget).length > 0)
		// {
		// 	$(modalTarget).modal('show');
		// }else{
			$.ajax({
				url : url,
				type : 'POST',
				success : function(html){
					$('body').append(html);
					$(modalTarget).modal('show');
  				}
		    });
		//}
		return false;
	});


	$(document).on('click', '.submit-btn', function(){
        
		btn = $(this);
		form = btn.parents('.form');

		if(form.find('#details').length)
		{  
			//ckeditor input
			form.find('#details').val(CKEDITOR.instances.details.getData());
		}

		url  = form.attr('action');
		data = new FormData(form[0]);

		formResults = form.find('#form-results');
		$.ajax({
				url : url,
				type : 'POST',
				data : data,
				dataType : 'json',
				beforeSend : function(){
                    formResults.removeClass().addClass('alert alert-info').html('loading...');
				},
				success : function(results){

					if(results.errors)
					{
						formResults.removeClass().addClass('alert alert-danger').html(results.errors);
					}else{
						formResults.removeClass().addClass('alert alert-success').html(results.success);
					}
					if(results.redirectTo)
					{
						window.location.href = results.redirectTo;
					}
				},
				cache : false,
				processData : false,
				contentType : false,
		    });
		return false;
	});

	//delete alert
	$('.delete').on('click',function(e){
		var result = confirm("Are you sure you want to delete ?");
        if(!result) {
            e.preventDefault();
        }
	});
</script>
</body>
</html>