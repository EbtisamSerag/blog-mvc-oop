  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo url('/admin')?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo url('/admin/categories')?>"><i class="fa fa-folder"></i> Categories</a></li>
        <li class="active">Update Category</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
          <div class="col-sm-12">
              <div class="box" id="users-list">
                <div class="box-header with-border">
                  <h1 class="">Edit <?php echo $category->name?></h1>
                  
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <form action="<?php echo url('admin/categories/save/' . $category->id)?>"
                    method="POSt" enctype=mulitpart/form-data>
                    <?php if($errors){?>
                      <div class="alert alert-danger"><?php echo implode('<br>', $errors)?></div>
                    <?php } ?>
                       <div id="form-results"></div>
                        <div class="form-group col-sm-6">
                          <label for="cat">Category name</label>
                          <input type="text" class="form-control" id="cat" name="name" placeholder="Categorty name" value="<?php echo $category->name?>">
                        </div>
                      <div class="form-group col-sm-6">
                          <label for="status">Status</label>
                          <select class="form-control" name="status" id="status">
                              <option value="enabled" <?php echo $category->status == 'enabled' ?'selected' : false?>>Enabled </option>
                              <option value="disabled" <?php echo $category->status == 'disabled' ?'selected' : false?>>Disabled</option>
                          </select>
                      </div>
                      <button class="btn btn-info">Submit</button>
                    </form>
                  
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                  <!-- <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">&laquo;</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">&raquo;</a></li>
                  </ul> -->
                </div>
              </div>
          </div>
      </div>
      

    </section>