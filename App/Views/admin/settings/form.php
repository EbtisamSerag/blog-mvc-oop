  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo url('/admin')?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Settings</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
          <div class="col-sm-12">
              <div class="box" id="users-list">
                <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-cogs"></i> Settings </h3>
                  
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                   <form action="<?php echo $action;?>"
                    method="POST">
                    <?php if($success){?>
                      <div class="alert alert-success"><?php echo  $success?></div>
                    <?php } ?>
                    <?php if($errors){?>
                      <div class="alert alert-danger"><?php echo implode('<br>', $errors)?></div>
                    <?php } ?>
                       
                        <div class="form-group col-sm-12">
                          <label for="site_name">Site Name</label>
                          <input type="text" class="form-control" id="site_name" name="site_name" placeholder="Site name" value="<?php echo $site_name?>">
                        </div>

                        <div class="form-group col-sm-12">
                          <label for="site_email">Site Email</label>
                          <input type="email" class="form-control" id="site_email" name="site_email" placeholder="Site email" value="<?php echo $site_email?>">
                        </div>

                      <div class="form-group col-sm-12">
                          <label for="status">Site Status</label>
                          <select class="form-control" name="status" id="status">
                              <option value="enabled" <?php echo $site_status == 'enabled' ?'selected' : false?>>Enabled </option>
                              <option value="disabled" <?php echo $site_status == 'disabled' ?'selected' : false?>>Disabled</option>
                          </select>
                      </div>

                      <div class="form-group col-sm-12">
                          <label for="message">Site Close Message</label>
                          <textarea class="form-control message" name="message" id="message"><?php echo htmlspecialchars_decode($site_close_msg)?></textarea>
                      </div>

                      <button class="btn btn-info">Submit</button>
                    </form>
                                   
                  
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                  
                </div>
              </div>
          </div>
      </div>
      

    </section>
      