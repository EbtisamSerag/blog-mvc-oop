<?php
use System\Application;

if(!function_exists('pre'))
{
   function pre($var)
   {
   	echo '<pre>';
   	print_r($var);
   	echo '</pre>';
   }
}
if(!function_exists('array_get'))
{
	function array_get($array ,$key,$default =null)
	{
		return isset($array[$key])? $array[$key] : $default;
	}
}

if(!function_exists('_e'))
{
   //escape the given value
   function _e($value)
   {
      return htmlspecialchars($value);
   }
}

if(!function_exists('assets'))
{
   //generate full path from given path in public directory
   function assets($path)
   {
      $app = Application::getInstance();
      return $app->url->link('public/' . $path);
   }
}

if(!function_exists('url'))
{
   //generate full path from given path 
   function url($path)
   {
      $app = Application::getInstance();
      return $app->url->link($path);
   }
}

if(!function_exists('read_more'))
{      
   /**
   *cut the given string and get the given number of words
   *@param string $string
   *@param int $number_of_word
   *@return string
   */
   function read_more($string, $number_of_word)
   {
      //remove any empty value in string
      $words_of_string = array_filter(explode(' ', $string));

      if(count($words_of_string) <= $number_of_word)
      {
         return $string;
      }
      $cut_string_words = implode(' ', array_splice($words_of_string, 0,$number_of_word));
      return $cut_string_words; 
   }
}
if(!function_exists('seo'))
{
   /**
   *removing unwanted characters from the given string and replace it with -
   *@param string $string
   *@return string
   */
   function seo($string)
   {
      //remove any space from beginning and the end
      $string = trim($string);
      //replace any non english and numeric characters and dashes to space
      $string = preg_replace('#[^\w]#', ' ', $string);
      //replace any mutli white space to one space
      //$string = preg_replace('#[^\s]+#', ' ', $string);
      //replace space with dash 
      $string = str_replace(' ', '-', $string);
      return trim(strtolower($string),'-');
   }
}


?>