<?php
namespace System;
class Url
{
	protected $app;

	public function __construct(Application $app)
	{
		$this->app = $app;
	}

	public function __get($key)
	{
		return $this->app->get($key);
	}

	/**
	*
	*generate full link for the given path
	*/
	public function link($path)
	{
		return $this->app->request->baseUrl() . trim($path,'/');
	}

	/**
	*
	*redirect to the given path
	*/
	public function redirectTo($path)
	{
		header('location:' . $this->link($path));
		exit;
	}
}
?>