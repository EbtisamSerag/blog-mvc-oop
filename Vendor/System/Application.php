<?php
namespace System;
use Closure;
class Application
{
	private $container = [];

	private static $instance;

	private function __construct(File $file)
	{
		$this->share('file',$file);
		$this->registerClasses();
		$this->loadHelpers();
	}
    
    public function getInstance($file = null)
    {
    	if(is_null(static::$instance))
    	{
    		static::$instance = new static($file);
    	}
    	return static::$instance;
    }

	public function run()
	{
		$this->session->start();
		$this->request->prepareUrl();
		$this->file->call('App/index.php');
		list($controller,$method,$arguments) = $this->route->getProperRoute();
		if($this->route->hasCallsFirst())
		{
			$this->route->callFirstCalls();
		}
		$output = (string) $this->load->action($controller,$method,$arguments);
		$this->response->setOutput($output);
		$this->response->send();
	}

	private function registerClasses()
	{
		spl_autoload_register([$this,'load']);
	}
	/**
    *get all core classes with its aliases
    *
	*/
	private function coreClasses()
	{
		return [
		     'request'       => 'System\\HTTP\\request',
		     'response'      => 'System\\HTTP\\response',
		     'route'         => 'System\\Route',		     
		     'session'       => 'System\\Session',
		     'cookie'        => 'System\\Cookie',
		     'load'          => 'System\\Loader',
		     'html'          => 'System\\Html',
		     'url'           => 'System\\Url',
		     'validator'     => 'System\\Validation',
		     'pagination'    => 'System\\Pagination',		     		     
		     'db'            => 'System\\Database',
		     'view'          => 'System\\View\\viewFactory',
		];
	}

	public function share($key,$value)
	{
		if($value instanceof Closure)
		{
			$value = call_user_func($value,$this);
		}
		$this->container[$key] = $value;
	}
	public function __get($key)
	{
		return $this->get($key);
	}

	public function load($class)
	{
		if(strpos($class, 'App') === 0)
		{
			$file = $class . '.php';
		}else{
			//get the class from vendor
			$file = 'vendor/' . $class . '.php';			
		}
		if($this->file->exists($file))
			{
				$this->file->call($file);
			}
	}

	public function get($key)
	{
		if(! $this->isSharing($key))
		{
			if($this->isCoreAlias($key))
			{
				$this->share($key,$this->createNewCoreObject($key));
			}else{
				die('<b>' . $key . '</b> Not found in container application');
			}
		}
		return $this->container[$key];
	}

	private function isCoreAlias($alias)
	{
		$coreClasses = $this->coreClasses();
		return isset($coreClasses[$alias]);
	}

	private function createNewCoreObject($alias)
	{
		$coreClasses = $this->coreClasses();
		$object = $coreClasses[$alias];
		return new $object($this); 
	} 

	public function isSharing($key)
	{
		return isset($this->container[$key]);
	}

	private function loadHelpers()
	{
		$this->file->call('vendor/helpers.php');
	}
}
?>