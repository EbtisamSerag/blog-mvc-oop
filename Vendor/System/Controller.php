<?php
namespace System;
abstract class Controller
{
	protected $app;

	/**
	*errors container
	*@var array
	*/
	protected $errors = [];

	public function __construct(Application $app)
	{
		$this->app = $app;
	}
    
    /**
    *encode the given value in json
    *@param mixed data
    *@return string
    */
    public function json($data)
    {
    	$this->json_encode($data);
    }

	public function __get($key)
	{
		return $this->app->get($key);
	}


}
?>