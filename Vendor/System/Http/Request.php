<?php
namespace System\HTTP;
class Request
{
	private $url;
	private $baseUrl;

	/**
	*uploaded files container
	*@var array
	*/
	private $files = [];

	public function prepareUrl()
	{
		$script = dirname($this->server('SCRIPT_NAME'));
		$requestUri = $this->server('REQUEST_URI');
        if(strpos($requestUri, '?') !== false)
        {
        	list($requestUri,$queryString) = explode('?',$requestUri);
        }
		$this->url = rtrim(preg_replace('#^'.$script.'^#', '', $requestUri),'/');
		if(! $this->url)
		{
			$this->url = '/';
		}
		
		$this->baseUrl = $this->server('REQUEST_SCHEME') .'://' . $this->server('HTTP_HOST') . '/';

	}

	public function server($key,$default = null)
	{
		return array_get($_SERVER,$key,$default);
	}

	public function url()
	{
		return $this->url;
	}

	public function post($key,$default = null)
	{
		$value = array_get($_POST,$key,$default);
		if(is_array($value))
		{
			$value = array_filter($value);
		}else{
			//ust remove any white space if there is value
			$value = trim($value);
		}
		
		return $value;
	}

	/**
    *set value to _Post from the given key
	*@param string $key
	*@param mixed $value
	*@return mixed
	*/
	public function setPost($key,$value)
	{
		$_POST[$key] = $value;
	}

	/**
	*get the uploaded file object for the given input
	*@param string $input
	*@return \System\Http\UploadedFile
	*/
	public function file($input)
	{
		if(isset($this->files[$input]))
		{
			return $this->files[$input];
		}

		$uploadedFile = new UploadedFile($input);
		$this->files[$input] = $uploadedFile;
		return $this->files[$input];
	}
	/**
	*get the referer link
	*@return string
	*/
	public function referer()
	{
		return $this->server('HTTP_REFERER');
	}

	public function get($key,$default = null)
	{
		$value = array_get($_GET,$key,$default);
		if(is_array($value))
		{
			$value = array_filter($value);
		}else{
			//ust remove any white space if there is value
			$value = trim($value);
		}
		return $value;
	}
	public function method()
	{
		return $this->server('REQUEST_METHOD');
	}

	public function baseUrl()
	{
		return $this->baseUrl;
	}
	
}
?>