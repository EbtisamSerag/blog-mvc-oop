<?php
namespace System\Http;

use System\Application;
class UploadedFile 
{
	private $app;

	/**
	*the uploaded file
	*@var array
	*/
	private $file = [];

	/**
	*the uploaded file name
	*@var string
	*/
	private $fileName ;

	/**
	*the uploaded file name without extension
	*@var string
	*/
	private $nameOnly ;

	/**
	*the uploaded file mime type
	*@var string
	*/
	private $mimeType ;

	/**
	*the uploaded temp file path
	*@var string
	*/
	private $tempFile ;

	/**
	*the uploaded file size in btes
	*@var int
	*/
	private $size ;

	/**
	*the uploaded file error
	*@var string
	*/
	private $error ;

	/**
	*constructor
	*@param string $input
	*/
	public function __construct($input)
	{
		$this->getFileInfo($input);
	}

	/**
	*start collecting uploaded file data
	*@param string  $input
	*@return void
	*/
	private function getFileInfo($input)
	{
		if(empty($_FILES[$input]))
		{
			return;
		}
		$file = $_FILES[$input];
		$this->error = $file['error'];
		if($this->error != UPLOAD_ERR_OK)
		{
			return;
		}
	    
	    $this->file = $file;
		$this->fileName = $this->file['name'];

		$fileNameInfo = pathinfo($this->fileName);
		$this->nameOnly = $fileNameInfo['filename'];
		$this->extension = strtolower($fileNameInfo['extension']);

		$this->mimeType = $file['type'];
		$this->tempFile = $file['tmp_name'];
		$this->size = $file['size'];

	}

	/**
    *determine if the file uploaded
	*@return bool
	*/
	public function exists()
	{
		return !empty($this->file);
	}

	/**
    *get file name
	*@return string
	*/
	public function getFileName()
	{
		return $this->fileName;
	}

	/**
    *get file name only without extension 
	*@return string
	*/
	public function getNameOnly()
	{
		return $this->nameOnly;
	}

	/**
    *get file extension 
	*@return string
	*/
	public function getExtension()
	{
		return $this->extension;
	}

	/**
    *get file mime type
	*@return string
	*/
	public function getMimeType()
	{
		return $this->mimeType;
	}

	/**
    *determine if uploaded file is image
	*@return bool
	*/
	public function isImage()
	{
		$imageExtension = ['gif','jpg','png','jpeg'];
		return strpos($this->mimeType, 'image/') === 0 AND in_array($this->extension, $imageExtension);
	}

	/**
	*movethe uploaded file to given destination target  
	*@param string $target
	*@param string $newFileName
	*@return string
	*/
	public function moveTo($target,$newFileName = null)
	{
		$fileName = $newFileName ?: sha1(mt_rand()). '_' .sha1(mt_rand());
		$fileName .= '.' .$this->extension;
		if(! is_dir($target))
		{
			mkdir($target,0777,true);
		}
		$uploadFilePath = rtrim($target,'/'). '/' . $fileName;
		move_uploaded_file($this->tempFile, $uploadFilePath);
		return $fileName;
	}
}

?>