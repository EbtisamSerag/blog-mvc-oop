<?php
namespace System;

class Route
{
	private $app;

	private $routes = [];

	/**
	*current route 
	*@var array
	*/
	private $current = [];
	
	/**
	*calls container
	*@var array
	*/
	private $calls = [];

	private $notFound;

	public function __construct(Application $app)
	{
		$this->app = $app;
	}

	public function add($url,$action,$requestMethod = 'GET')
	{
		$route = [
			'url'       => $url,
			'pattern'   => $this->generatePattern($url),
			'action'    => $this->getAction($action),
			'method'    => $requestMethod,
		];
		$this->routes[] = $route; 
	}

	private function generatePattern($url)
	{
		$pattern ='#^';

		$pattern .= str_replace([':text' ,':id'], ['([a-zA-A0-9-]+)','(\d+)'], $url);

		$pattern .= '$#';

		return $pattern;
	}

	private function getAction($action)
	{
		$action = str_replace('/', '\\', $action);
		return strpos($action, '@') !== false ?$action : $action . '@index';
	}

	public function notFound($url)
	{
		$this->notFound = $url;
	}

	/**
	*call the given callback before calling the main controller
	*@var callable $callableb
	*@return $this
	*/
	public function callFirst(callable $callable)
	{
		$this->calls['first'][] = $callable;
		return $this;
	}

	/**
	*detemine if there is an callbacks that will be called before
	*calling the main controller
	*@return bool
	**/
	public function hasCallsFirst()
	{
		return ! empty($this->calls['first']);
	}

	/**
	*call all callbacks that will be called before
	*calling the main controller
	*@return bool
	**/
	public function callFirstCalls()
	{
		foreach($this->calls['first'] as $callback)
		{
			call_user_func($callback,$this->app);
		}
	}

	public function getProperRoute()
	{
		foreach($this->routes as $route){
			if($this->isMatching($route['pattern']) AND $this->isMatchingRequestMethod($route['method']))
			{
				$arguments = $this->getArgumentsFrom($route['pattern']);
                list($controller,$method) = explode('@', $route['action']);
                $this->current = $route; 
                return [$controller,$method,$arguments];
			}
		}
		return $this->app->url->redirectTo($this->notFound);
	}

	/**
	*get current route url
	*@return string
	*/
	public function getCurrentRouteUrl()
	{
		return $this->current['url'];
	}

	private function isMatching($pattern)
	{
		return preg_match($pattern, $this->app->request->url());
	}

	/**
	*determine if current request method equals
	*the given route method
	*@param string $routeMethod
	*@return bool
	*/
	private function isMatchingRequestMethod($routeMethod)
	{
		return $routeMethod == $this->app->request->method();
	}

	private function getArgumentsFrom($pattern)
	{
		preg_match($pattern, $this->app->request->url(),$matches);
		array_shift($matches);
		return $matches;
	}

	/**
	*get all routes
	*@return array
	*/
	public function routes()
	{
		return $this->routes;
	}
}

?>