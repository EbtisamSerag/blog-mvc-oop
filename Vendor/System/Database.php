<?php
namespace System;
use PDO;
use PDOException;
class Database
{
	private $app;
	private static $connection;
    //table name
	private $table;
	//data container
	private $data = [];
	private $bindings = [];

	private $lastId;
	private $wheres = [];
	private $selects = [];
	private $joins = [];
	private $rows = 0;
	private $limit;
	private $offset;
	private $orderBy = [];
    
    /**
    *havings
    *@var  array 
    */ 
    private $havings = [];
    
    /**
    *group by
    *@var  array 
    */ 
    private $groupBy = [];
    

	public function __construct(Application $app)
	{
		$this->app = $app;
		if(! $this->isConnected())
		{
			$this->connect();
		}
	}

	private function isConnected()
	{
		return static::$connection instanceof PDO;
	}

	private function connect()
	{
		$connectionData = $this->app->file->call('config.php');
		extract($connectionData);
        try
        {
        	static::$connection = new PDO('mysql:host=' . $server .';dbname=' . $dbname,$dbuser,$dbpass);
        	static::$connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        	static::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        	static::$connection->exec('SET NAMES utf8');
        }catch (PDOException $e){
        	die($e->getMessage());
        }		
	}

	public function connection()
	{
		return static::$connection;
	}

	public function select( ...$selects)
	{
		$selects = func_get_args();
		$this->selects = array_merge($this->selects,$selects);
		return $this;
	}
     
    public function join($join)
	{
		$this->joins[] = $join;
		return $this;
	} 

	public function limit($limit, $offset = 0)
	{
		$this->limit = $limit;
		$this->offset = $offset;

		return $this;
	}

	public function fetch($table = null)
	{
		if($table)
		{
			$this->table($table);
		}
		$sql = $this->fetchStatment();
		$result = $this->query($sql,$this->bindings)->fetch();
		$this->reset();
		return $result;
	}

	public function fetchAll($table = null)
	{
		if($table)
		{
			$this->table($table);
		}
		$sql = $this->fetchStatment();
		$query = $this->query($sql ,$this->bindings);
		$results = $query->fetchAll();
		$this->rows = $query->rowCount();
		$this->reset();
		return $results;
	}

	public function rows()
	{
		return $this->rows;
	}

	private function fetchStatment()
	{
		$sql = 'SELECT ';
		if($this->selects)
		{
			$sql .= implode(',', $this->selects);
		}else{
			$sql .= '*';
		}
		$sql .= ' FROM ' . $this->table . ' ';
		if($this->joins)
		{
			$sql .= implode(' ', $this->joins).' ';
		}
		if($this->wheres)
		{
			$sql .= 'WHERE ' . implode(' ',$this->wheres);
		}
		if($this->havings)
		{
			$sql .= 'HAVING ' . implode(' ',$this->havings);
		}
		if($this->orderBy)
		{
			$sql .= ' ORDER BY ' . implode(' ', $this->orderBy);
		}
		if($this->limit)
		{
			$sql .= ' LIMIT ' . $this->limit;
		}
		if($this->offset)
		{
			$sql .= ' OFFSET ' . $this->offset;
		}
		
		if($this->groupBy)
		{
			$sql .= 'GROUP BY ' . implode(' ',$this->groupBy);
		}
		return $sql;
	}

	public function orderBy($orderBy,$sort = 'ASC')
	{
		$this->orderBy = [$orderBy , $sort];
		return $this;
	}

	/**
	*set the table name
	*/
	public function table($table)
	{
		$this->table = $table;
		return $this;
	}

	public function from($table)
	{
		return $this->table($table);
		 
	}
	public function data($key, $value = null)
	{
		if(is_array($key))
		{
			$this->data = array_merge($this->data ,$key);
			$this->addToBindings($key);
		}else{
			$this->data[$key] = $value;
			$this->addToBindings($value);
		}		
		return $this;
	}
	public function insert($table = null)
	{
		if($table)
		{
			$this->table($table);
		}
		$sql = 'INSERT INTO '. $this->table . ' SET ';

		$sql .= $this->setField();
		
			$this->query($sql,$this->bindings);
			$this->lastId = $this->connection()->lastInsertId();
			$this->reset();
			return $this;

		
	}

	public function update($table = null)
	{
		if($table)
		{
			$this->table($table);
		}
		    $sql = 'UPDATE '. $this->table . ' SET ';
		    $sql .= $this->setField();
            
			if($this->wheres){
				$sql .= 'WHERE ' . implode(' ',$this->wheres);
			}
			
			$this->query($sql,$this->bindings);
			$this->lastId = $this->connection()->lastInsertId();
			$this->reset();
			return $this;
	}

	private function setField()
	{
		$sql = '';
		foreach ($this->data as $key => $value) {
			$sql .='`' . $key . '` = ? ,';
		}	
		$sql = rtrim($sql, ',');
		return $sql;
	}

	public function where()
	{
		$bindings = func_get_args();
		$sql = array_shift($bindings);
		$this->addToBindings($bindings);
		$this->wheres[] = $sql;
		return $this;
	}
    
    /**
    *having clause
    *@return $this
    */
    public function having()
	{
		$bindings = func_get_args();
		$sql = array_shift($bindings);
		$this->addToBindings($bindings);
		$this->havings[] = $sql;
		return $this;
	}

	/**
    *group by clause
    *@param array $arguments php=>5.6
    *@return $this
    */
    public function groupBy(...$arguments)
	{
		$this->groupBy = $arguments;
		return $this;
	}

	public function lastInsertId()
	{
		return $this->lastId;
	}

	private function addToBindings($value)
	{
		if(is_array($value))
		{
			$this->bindings = array_merge($this->bindings,array_values($value));
		}else{
			$this->bindings[] = $value;
		}
		
	}

	public function query()
	{
		$bindings =func_get_args();
		$sql = array_shift($bindings);
		if(count($bindings) == 1 AND is_array($bindings[0]))
		{
			$bindings = $bindings[0];
		}
		try
		{
			$query = $this->connection()->prepare($sql);
			foreach ($bindings as $key => $value) {
				$query->bindValue($key + 1, _e($value));
			}
			$query->execute();
			return $query;
        }catch(PDOException $e){
        	die($e->getMessage());
        }	
	}

	public function delete($table = null)
	{
		if($table)
		{
			$this->table($table);
		}
		    $sql = 'DELETE FROM '. $this->table . ' ';
            
			if($this->wheres){
				$sql .= 'WHERE ' . implode('',$this->wheres);
			}
			
			$this->query($sql,$this->bindings);
            $this->reset();
			return $this;
	}

	private function reset()
	{
		$this->data = [];
		$this->bindings = [];
		$this->selects = [];
		$this->joins = [];
		$this->wheres = [];
		$this->orderBy = [];
		$this->havings = [];	
		$this->groupBy = [];		
		$this->limit = null;
		$this->offset = null;
		$this->table = null;

		



	}
}
?>
