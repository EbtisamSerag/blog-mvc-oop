<?php
namespace System;
abstract class Model
{
	protected $app;
	protected $table;

	public function __construct(Application $app)
	{
		$this->app = $app;
	}

	public function __get($key)
	{
		return $this->app->get($key);
	}

	public function __call($method,$args)
	{
		return call_user_func_array([$this->app->db,$method], $args);
	}

	/**
	*
	*get all model records
	*/
	public function all()
	{
		return $this->fetchAll($this->table);
	}

	/**
	*
	*get record by id
	*/
	public function get($id)
	{
		return $this->where('id = ?', $id)->fetch($this->table);
	}

	/**
	*determine if the given value of the ey exists in table
	*@param mixed $value
	*@param string $key
	*@return bool
	*/
	public function exists($value, $key = 'id')
	{
		return (bool) $result = $this->select($key)->where($key . '= ?' ,$value)->fetch($this->table);
		
	}

	/**
	*delete record by id 
	*@param int  $id	
	*@return void
	*/
	public function delete($id)
	{
		return $this->where('id = ?' ,$id)->delete($this->table);
		
	}
}
?>