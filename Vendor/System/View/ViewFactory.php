<?php
namespace System\View;
use System\Application;

class ViewFactory
{
	private $app;

	public function __construct(Application $app)
	{
		$this->app = $app;
	}

    //generate view
	public function render($viewPath, array $data = [])
	{
		return new view($this->app->file,$viewPath,$data);
	}
}
?>