<?php
namespace System\View;


interface  ViewInterface
{
	/**
	*get the view output
	*
	*/
	public function getOutput();

    /**
    *convert the view object to string in printing
    *i.e echo $object
    */
    public function __toString();
}	

?>