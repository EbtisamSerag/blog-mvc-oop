<?php
namespace System;
class Validation
{
	/**
	*application object
	*@var \System\Application
	*/
	private $app;

	/**
    *Errors container
	*@var array
	*/
	private $errors = [];

	/**
	*constructor
	*@param \System\Application $app
	*/
	public function __construct(Application $app)
	{
		$this->app = $app;
	}

	/**
	*determine if the given input is not empty
	*@param string $inputName
	*@param string $customErrorMessage
	*@return $this
	*/
	public function required($inputName,$customErrorMessage = null )
	{
		if($this->hasErrors($inputName))
		{
			return $this;
		}
		$inputValue = $this->value($inputName);
		if($inputValue === '')
		{
			$message = $customErrorMessage ?: sprintf('%s Is Required',ucfirst($inputName));
			$this->addError($inputName,$message);
		}
	    return $this; 
	}

	/**
	*determine if the given input is valid email
	*@param string $inputName
	*@param string $customErrorMessage
	*@return $this
	*/
	public function email($inputName,$customErrorMessage = null )
	{
		if($this->hasErrors($inputName))
		{
			return $this;
		}
		$inputValue = $this->value($inputName);
		if(!filter_var($inputValue , FILTER_VALIDATE_EMAIL))
		{
			$message = $customErrorMessage ?: sprintf('%s Is Not Valid Email',ucfirst($inputName));
			$this->addError($inputName,$message);
		}
		return $this;
	}

	/**
	*determine if the given input has float value
	*@param string $inputName
	*@param string $customErrorMessage
	*@return $this
	*/
	public function float($inputName,$customErrorMessage = null )
	{
		if($this->hasErrors($inputName))
		{
			return $this;
		}
		$inputValue = $this->value($inputName);
		if(! is_float($inputValue)) 
		{
			$message = $customErrorMessage ?: sprintf('%s Shoud Be Float ',ucfirst($inputName));
			$this->addError($inputName,$message);
		}
		return $this;
	}

	/**
	*determine if the given input length should be at least the given length
	*@param string $inputName
	*@param int $length
	*@param string $customErrorMessage
	*@return $this
	*/
	public function minLen($inputName,$length,$customErrorMessage = null )
	{
		if($this->hasErrors($inputName))
		{
			return $this;
		}
		$inputValue = $this->value($inputName);
		if(strlen($inputValue) < $length) 
		{
			$message = $customErrorMessage ?: sprintf('%s Shoud Be At Least %d',ucfirst($inputName),$length);
			$this->addError($inputName,$message);
		}
		return $this;
	}

	/**
	*determine if the given input length should be at most the given length
	*@param string $inputName
	*@param int $length
	*@param string $customErrorMessage
	*@return $this
	*/
	public function maxLen($inputName,$length,$customErrorMessage = null )
	{
		if($this->hasErrors($inputName))
		{
			return $this;
		}
		$inputValue = $this->value($inputName);
		if(strlen($inputValue) > $length) 
		{
			$message = $customErrorMessage ?: sprintf('%s Shoud Be At Most %d',ucfirst($inputName),$length);
			$this->addError($inputName,$message);
		}
		return $this;
	}

	/**
	*determine if the given first input matches the second input
	*@param string $firstInput
	*@param string $secondInput
	*@param string $customErrorMessage
	*@return $this
	*/
	public function match($firstInput,$secondInput,$customErrorMessage = null )
	{
		$firstInputValue = $this->value($firstInput);
		$secondInputValue = $this->value($secondInput);

		if($firstInputValue != $secondInputValue)
		{
			$message = $customErrorMessage ?: sprintf('%s Is Not Matched with %s',ucfirst($firstInput),ucfirst($secondInput));
			$this->addError($secondInput,$message);
		} 
		return $this;

	}

	/**
	*determine if the given input unique in database
	*@param string $inputName
	*@param array $databaseData
	*@param string $customErrorMessage
	*@return $this
	*/
	public function unique($inputName,array $databaseData,$customErrorMessage = null )
	{
		if($this->hasErrors($inputName))
		{
			return $this;
		}
		$inputValue = $this->value($inputName);

		$table = null;
		$column = null;
		$exceptionColumn = null;
		$exceptionColumnValue = null;

		if(count($databaseData) == 2)
		{
			list($table, $column) = $databaseData;
		}elseif(count($databaseData) == 4)
		{
			list($table, $column, $exceptionColumn, $exceptionColumnValue) = $databaseData;
		}
		if($exceptionColumn)
		{
			$result = $this->app->db->select($column)
		                           ->from($table)
		                           ->where($column . '= ?' AND $exceptionColumn .'!= ?' ,$inputValue, $exceptionColumnValue)
		                           ->fetch();

		}else{
			$result = $this->app->db->select($column)
		                           ->from($table)
		                           ->where($column . '=?' ,$inputValue)
		                           ->fetch();
		}
		

		if($result)
		{
			$message = $customErrorMessage ?: sprintf('%s Alread Exists',ucfirst($inputName));
			$this->addError($inputName,$message);
		}
	}

	/**
	*determine if the given input file exists
	*@param string $inputName
	*@param string $customErrorMessage
	*@return $this
	*/
	public function requiredFile($inputName ,$customErrorMessage = null)
	{
		if($this->hasErrors($inputName))
		{
			return $this;
		}
		$file = $this->app->request->file($inputName);
		if(!$file->exists())
		{
			$message = $customErrorMessage ?: sprintf('%s Is Required',ucfirst($inputName));
			$this->addError($inputName,$message);
		}
		return $this;
	}


	/**
	*determine if the given input file is an image
	*@param string $inputName
	*@param string $customErrorMessage
	*@return $this
	*/
	public function image($inputName ,$customErrorMessage = null)
	{
		if($this->hasErrors($inputName))
		{
			return $this;
		}
		$file = $this->app->request->file($inputName);
        if(!$file->exists())
        {
        	return $this;
        }

		if(!$file->isImage())
		{
			$message = $customErrorMessage ?: sprintf('%s Is Not Valid Image',ucfirst($inputName));
			$this->addError($inputName,$message);
		}
		return $this;
	}


	/**
	*add custom message
	*@param strin $message
	*@return $this
	*/
	public function message($message)
	{
		$this->errors[] = $message;
		return $this;
	}

	/**
	*validate all inputs
	*
	*@return $this
	*/
	public function validate()
	{}

	/**
	*determine if there are any invalid inputs 
	*
	*@return bool
	*/
	public function fails()
	{
		return !empty($this->errors);
	}

	/**
	*determine if all inputs are valid 
	*
	*@return bool
	*/
	public function passes()
	{
		return empty($this->errors);
	}

	/**
	*get all errors 
	*
	*@return array
	*/
	public function getMessages()
	{
		return $this->errors;
	}

	/**
	*flatten errors and make it as string imploded with break
	*
	*@return string
	*/
	public function flattenMessages()
	{
		return implode('<br>', $this->errors);
	}

	/**
	*get the value of the given input name
	*@param string $input
    *@return mixed
	*/
	private function value($input)
	{
		return $this->app->request->post($input);
	}

	/**
	*add input error
	*@param string $inputName
	*@param string $errorMessage
    *@return void
	*/
	private function addError($inputName, $errorMessage)
	{
		if(!array_key_exists($inputName, $this->errors))
		{
			$this->errors[$inputName] = $errorMessage;
		}
	}
	/**
	*detemine if the given input has previous error
	*@param string $inputName
	*/
	private function hasErrors($inputName)
	{
		return array_key_exists($inputName, $this->errors);
	}
}

?>