<?php
namespace System;

class Pagination
{
	/**
	*application object 
	*
	*/
	private $app;
	/**
	*total item 
	*@var int
	*/
	private $totalItems;

	/**
	*items per page 
	*@var int
	*/
	private $itemsPerPage = 1;

	/**
	*last page nubmer
	*@var int
	*/
	private $lastPage;

	/**
	*current page number
	*@var int
	*/
	private $page = 1;

	public function __construct(Application $app)
	{
		$this->app = $app;
		$this->setCurrentPage();
	}

	/**
	*set current page
	*@return void
	*/
	private function setCurrentPage()
	{
		$page = $this->app->request->get('page');

		if(!is_numeric($page) OR $page < 1)
		{
			$page = 1;
		}
		$this->page = $page;
	}


	/**
	*get current page number
	*@return int
	*
	**/
	public function Page()
	{
		return $this->page;
	}

	/**
	*get items per  page 
	*@return int
	*
	**/
	public function itemsPerPage()
	{
		return $this->itemsPerPage;
	}

	/**
	*get total items  
	*@return int
	*
	**/
	public function totalItems()
	{
		return $this->totalItems;
	}

	/**
	*set total items 
	*@param int $totalItems 
	*@return $this
	*/
	public function setTotalItems($totalItems)
	{
		$this->totalItems = $totalItems;
		return $this;
	}

	/**
	*set items per page 
	*@param int $itemsPerPage 
	*@return $this
	*/
	public function setItemsPerPage($itemsPerPage)
	{
		$this->itemsPerPage = $itemsPerPage;
		return $this;
	}

	/**
	*start pagination
	*@return $this
	*/
	public function paginate()
	{
		$this->setLastPage();
		return $this;
	}

	/**
	*set last page 
	*@return void
	*/
	private function setLastPage()
	{
		$this->lastPage = ceil($this->totalItems .' / '. $this->itemsPerPage);
	}

	/**
	*get next page
	*@return int
	*/
	public function next()
	{
		return $this->page + 1;
	}

	/**
	*get previous page
	*@return int
	*/
	public function prev()
	{
		return $this->page - 1;
	}

	/**
	*get last page
	*@return int
	*/
	public function last()
	{
		return $this->lastPage;
	}
}
?>